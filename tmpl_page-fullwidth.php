<?php
/*
 Template Name: 	Full-Width Page
 Description:		Costum Page template
 Modified: 			27/01/2015
 Author:			Arber Braja
*/
?>

<?php get_header(); ?>

	<div id="primary" class="content-area col-sm-12 col-md-12">
		<main id="main" class="site-main" role="main">

			<?php
			while(have_posts()) : the_post();
				get_template_part('content', 'page');
			endwhile;
			?>

		</main>
	</div>

<?php get_footer(); ?>
