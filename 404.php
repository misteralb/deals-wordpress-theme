<?php
/*
 Template: 	Error 404 Page
 Modified: 	26/01/2015
 Author:	Arber Braja
*/
 ?>

<?php get_header(); ?>

	<div id="primary" class="content-area col-sm-12 col-md-12">
		<main id="main" class="site-main" role="main">
			<section class="error-404 not-found">
				<style>
					.dealtitle { display: none; }
				</style>
				<?php $type = get404Type();
				if($type == "deals") { ?>
				<h3><?php _e('Offerta non piu’ disponibile!', 'wpdeals'); ?></h3>
				<p><?php _e("Ci dispiace, l'offerta che cercavi è terminata. Per non perdere le nostre prossime segnalazioni, resta aggiornato iscrivendoti alla nostra newsletter.", 'wpdeals'); ?></p>
				<?php  subscribe_newsletter_horizontal(); ?>
				<h3><?php _e('Le nostre ultime offerte ...', 'wpdeals'); ?></h3>
				<?php echo do_shortcode("[deals posts='8']");
				} else { ?>
				<header class="page-header">
					<h1 class="page-title"><?php _e('Errore 404!', 'wpdeals'); ?></h1>
				</header>
				<div class="page-content">
					<p><?php _e('La pagina richiesta non esiste o non è più dispobilie', 'wpdeals'); echo "<br/>"; ?></p>
					<?php get_search_form(); ?>
				</div>
				<?php } ?>
			</section>
		</main>
	</div>

<?php get_footer(); ?>