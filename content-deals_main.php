<?php
/*
 Template: 	Content Deals Main
 Modified: 	26/01/2015
 Author:	Arber Braja
*/
?>

<div class="col-xs-12 col-sm-6 alldeals col-md-3">
    <div class="dealimg">
        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
            <?php deal_image(); ?>
        </a>
    </div>
    <div class="dealdetails">
        <h4><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h4>
    </div>
</div>