<?php get_header(); ?>

	<div id="primary" class="content-area image-attachment col-sm-12 col-md-12">
		<div id="content" class="site-content" role="main">

		<?php while(have_posts()) : the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<header class="entry-header">
					<h1 class="entry-title"><?php the_title(); ?></h1>
				</header>

				<div class="entry-content">
					<div class="entry-attachment">
						<div class="attachment">
							<?php
								$attachments = array_values(get_children(array(
									'post_parent'    => $post->post_parent,
									'post_status'    => 'inherit',
									'post_type'      => 'attachment',
									'post_mime_type' => 'image',
									'order'          => 'ASC',
									'orderby'        => 'menu_order ID'
								)));
								foreach($attachments as $k => $attachment) {
									if($attachment->ID == $post->ID) break;
								}
								$k++;
								if(count($attachments) > 1) {
									if(isset($attachments[$k])) { $next_attachment_url = get_attachment_link($attachments[$k]->ID); }
									else { $next_attachment_url = get_attachment_link($attachments[0]->ID); }
								} else { $next_attachment_url = wp_get_attachment_url(); }
							?>

							<a href="<?php echo $next_attachment_url; ?>" title="<?php the_title_attribute(); ?>" rel="attachment">
								<?php
									$attachment_size = apply_filters('unite_attachment_size', array(1200, 1200));
									echo wp_get_attachment_image($post->ID, $attachment_size);
								?>
							</a>
						</div>

					</div>
				</div>
			</article>
		<?php endwhile; ?>

		</div>
	</div>

<?php get_footer(); ?>