<?php
/*
 Template Name:   	ADSL Offers Page
 Description:		Costum Page template
 Modified:      	27/01/2015
 Author:      		Arber Braja
*/
?>

<?php

function importIOquery() {
	$url = "https://api.import.io/store/data/e5f0f5cf-89ca-4e83-8d09-c2d93b1bae3b/_query?input/webpage/url=http%3A%2F%2Fwww.facile.it%2Fadsl%2F7-mega.html%3Fid%3D134&_user=d9cd56eb-4fc0-4346-97cb-258ef39d5e12&_apikey=yKwxQB%2FmCtRic03s9iSns03kVJ0KOQ%2BUmoDGAL8zodFyFhqBdcSM60dqHUvUCsTCHTEIYrsC9aY4Mz8mqicEhA%3D%3D";
	$ch = curl_init($url);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_TIMEOUT, 4);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    $output = curl_exec($ch);
	return json_decode($output, false);
}

$first_name = $_POST['first_name'];
$last_name = $_POST['last_name'];
$usr_email = $_POST['usr_email'];
$usr_address = $_POST['usr_address'];
$usr_city = $_POST['usr_city'];
$usr_cap = $_POST['usr_cap'];
$usr_phone = $_POST['usr_phone'];
$usr_mobile = $_POST['usr_mobile'];

$full_name = $first_name . " " . $last_name;

$to = get_option('admin_email');
$subject = "Offerte ADSL per: " . $full_name;

$message = 	"Da: $full_name\n " .
			"E-mail: $usr_email\n" .
			"Indirizzo: $usr_address\n" .
			"Citta: $usr_city\n" .
			"CAP: $usr_cap\n" .
			"Telefono: $usr_phone\n" .
			"Mobile: $usr_mobile\n";

$headers = 'From: '. $usr_email . "\r\n" . 'Reply-To: ' . $usr_email . "\r\n";

if(isset($_POST['submitted'])) {
	$sent = wp_mail($to, $subject, strip_tags($message), $headers);
	if($sent) { $success = 1; }
}

?>

<?php get_header(); ?>

<div id="primary" class="content-area col-sm-12 col-md-8">
	<main id="main" class="site-main" role="main">

  	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
      	<header class="entry-header">
          	<h1 class="entry-title"><?php the_title(); ?></h1>
      	</header>

      	<div class="entry-content">

          	<div id="adsl-offers">

          		<div class="row heading">
          			<div class="col-md-12">
          				<p>OmaggiWeb compara le migliori offerte ADSL sempre aggiornate dal nostro staff e comparate in modo indipendente e imparziale.</p>
          				<p>Lascia i tuoi dati per essere contattato da un nostro consulente</p>
          				<span>I campi contrassegnati da * sono obbligatori</span>
          			</div>
          		</div>

          		<?php if($success == 1) : ?>
          		<div id="success" class="alert alert-success" role="alert">Grazie, i nostri consulenti si metteranno al piu presto possibile in contatto con voi!</div>
          		<?php endif; ?>

	          	<form id="show-offers-form" action="<?php the_permalink(); ?>" method="post">
	          		<div class="row">
	                  	<div class="col-md-6 col-sm-12">
	                    	<div class="form-group">
	                      		<label for="first_name">Nome: *</label>
	                      		<input class="form-control" type="text" name="first_name" id="first_name" required/>
	                    	</div>
	                  	</div>
	                  	<div class="col-md-6 col-sm-12">
	                    	<div class="form-group">
	                      		<label for="last_name">Cognome: *</label>
	                      		<input class="form-control" type="text" name="last_name" id="last_name" required/>
	                    	</div>
	                  	</div>
	                  	<div class="col-md-6 col-sm-12">
	                    	<div class="form-group">
	                      		<label for="usr_address">Indirizzo:</label>
	                      		<input class="form-control" type="text" name="usr_address" id="user_address" />
	                    	</div>
	                  	</div>
	                  	<div class="col-md-4 col-sm-12">
	                    	<div class="form-group">
	                      		<label for="usr_city">Citta: *</label>
	                      		<input class="form-control" type="text" name="usr_city" id="usr_city" required/>
	                    	</div>
	                  	</div>
	                  	<div class="col-md-2 col-sm-12">
	                    	<div class="form-group">
	                      		<label for="usr_cap">CAP:</label>
	                      		<input class="form-control" type="text" name="usr_cap" id="usr_cap" />
	                    	</div>
	                  	</div>
	                  	<div class="col-md-4">
	                  		<div class="form-group">
	                  			<label for="usr_email">E-mail: *</label>
	                  			<input class="form-control" type="email" name="usr_email" id="usr_email" required/>
	                  		</div>
	                  	</div>
	                  	<div class="col-md-4 col-sm-12">
	                    	<div class="form-group">
	                      		<label for="usr_phone">Telefono: *</label>
	                      		<input class="form-control" type="text" name="usr_phone" id="usr_phone" required/>
	                    	</div>
	                  	</div>
	                  	<div class="col-md-4 col-sm-12">
	                    	<div class="form-group">
	                      		<label for="usr_mobile">Mobile: *</label>
	                      		<input class="form-control" type="text" name="usr_mobile" id="usr_mobile" required/>
	                    	</div>
	                  	</div>
	                  	<div class="col-md-12">
	                  		<div class="checkbox">
							 	<label><input type="checkbox" name="privacy-consent" id="privacy-consent" value="">Accetto l'informativa e tutte le clausole indicate su <a href="http://www.omaggiweb.it/data-privacy/" title="" target="_blank">Data Privacy</a> di OmaggiWeb.</label>
							</div>
	                  	</div>
                  	</div>
	              	<input type="hidden" name="submitted" value="1">
	              	<p><input disabled id="showOffers" class="btn btn-success" type="submit" value="Visualizza Offerte ADSL"></p>
	         	</form>
	         	
          		<?php if($success == 1 || isset($_COOKIE['offers'])) : ?>

          		<script type="text/javascript">
          			jQuery.cookie('offers', 'active', { path: '/' });
          		</script>

          		<?php 

          		$result = importIOquery();
				$results = $result->results;

          		foreach($results as $result) {

					$clean_name = "jslogoop_image/_title";
					$operator_name = $result->$clean_name;
					$operator_logo = $result->jslogoop_image;
					$adsl_speed = $result->speed_number;
					$offer_name = $result->prodotto_text;
					$price_1 = $result->price_1;

					?>

					<div class="col-md-12 offers">
						<div class="col-md-3 col-sm-12 offer-img">
							<img src="<?php echo $operator_logo; ?>" class="img-responsive" alt="<?php echo $operator_name ?>" />
						</div>
						<div class="col-md-4 col-sm-12 offer-name">
							<span><?php echo $offer_name; ?></span>
						</div>
						<div class="col-md-3 col-sm-12 offer-speed">
							<span><?php echo $adsl_speed; ?> Mb/s</span>
						</div>
						<div class="col-md-2 col-sm-12 offer-price">
							<span><?php echo $price_1; ?> &euro;</span>
						</div>
					</div>

					<?php
				}
				endif;
          		?>
          	</div>

          	<script type="text/javascript">
          		var $j = jQuery.noConflict();
          		$j("#privacy-consent").on('change', function () {
          			var isChecked = $j('#privacy-consent').attr('checked')?true:false;
          			if(isChecked) { $j("#showOffers").prop("disabled", false); } 
          			else { $j("#showOffers").prop("disabled", true); }
          		});
          	</script>

          	<style>
          		#adsl-offers { margin-top: 20px; }
          		#adsl-offers #success { margin: 20px 0; }
          		#adsl-offers .heading { margin-bottom: 20px; }
          		#adsl-offers .heading span { font-color: #222; font-style: italic; }
          		#show-offers-form { font-size: 16px; font-family: Arial; color: #333; margin-bottom: 20px; }
          		.offers { width: 100%; border: 1px solid #ddd; padding: 10px; font-size: 22px; }
          		.offers .offer-img img { height: 80px; width: auto; }
          		.offers span { line-height: 80px; font-family: Arial; }
          		.offers .offer-name { text-align: center; border: 1px solid #eee; height: 80px; }
          		.offers .offer-speed { text-align: center; border: 1px solid #eee; height: 80px; }
          		.offers .offer-price { text-align: center; border: 1px solid #eee; height: 80px; }
          		.offers .offer-price span { font-weight: bold; color: #3A5795; }
          	</style>

      	</div>
  	</article>

</main>
</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>
