<?php

function unite_page_menu_args($args) {
	 $args['show_home'] = true;
	 return $args;
}
add_filter('wp_page_menu_args', 'unite_page_menu_args');


function unite_body_classes($classes) {
    if(is_multi_author()) { $classes[] = 'group-blog'; }
	return $classes;
}
add_filter('body_class', 'unite_body_classes');


function unite_wp_title($title, $sep) {
	global $page, $paged;
	if (is_feed()) { return $title; }
	$title .= get_bloginfo('name');

	$site_description = get_bloginfo('description', 'display');
	if ($site_description && (is_home() || is_front_page())) { $title .= " $sep $site_description"; }
	if ($paged >= 2 || $page >= 2) { $title .= " $sep " . sprintf( __('Page %s', 'unite'), max($paged, $page)); }
	return $title;
}
add_filter('wp_title', 'unite_wp_title', 10, 2);


// Mark Posts/Pages as Untiled when no title is used
add_filter('the_title', 'unite_title');

function unite_title($title) {
  if ($title == '') { return 'Untitled'; }
  else { return $title; }
}

// Prevent page scroll when clicking the more link
function unite_remove_more_link_scroll($link) {
  $link = preg_replace('|#more-[0-9]+|', '', $link);
  return $link;
}
add_filter('the_content_more_link', 'unite_remove_more_link_scroll');

// Change default "Read More" button when using the_excerpt
function unite_excerpt_more($more) {
  return ' <a class="more-link" href="'. get_permalink(get_the_ID()) . '">Continua con la lettura <i class="fa fa-chevron-right"></i></a>';
}
add_filter('excerpt_more', 'unite_excerpt_more');



function unite_setup_author() {
	global $wp_query;

	if ($wp_query->is_author() && isset($wp_query->post)) {
		$GLOBALS['authordata'] = get_userdata($wp_query->post->post_author);
	}
}
add_action('wp', 'unite_setup_author');



// Add Bootstrap classes for table
add_filter( 'the_content', 'unite_add_custom_table_class' );
function unite_add_custom_table_class( $content ) {
    return str_replace( '<table>', '<table class="table table-hover">', $content );
}

//Display social links
function unite_social(){
  $output = '<div id="social" class="social">';
  $output .= unite_social_item(of_get_option('social_facebook'), 'Facebook', 'facebook');
  $output .= unite_social_item(of_get_option('social_twitter'), 'Twitter', 'twitter');
  $output .= unite_social_item(of_get_option('social_google'), 'Google Plus', 'google-plus');
  $output .= unite_social_item(of_get_option('social_youtube'), 'YouTube', 'youtube');
  $output .= unite_social_item(of_get_option('social_linkedin'), 'LinkedIn', 'linkedin');
  $output .= unite_social_item(of_get_option('social_pinterest'), 'Pinterest', 'pinterest');
  $output .= unite_social_item(of_get_option('social_feed'), 'Feed', 'rss');
  $output .= unite_social_item(of_get_option('social_tumblr'), 'Tumblr', 'tumblr');
  $output .= unite_social_item(of_get_option('social_flickr'), 'Flickr', 'flickr');
  $output .= unite_social_item(of_get_option('social_instagram'), 'Instagram', 'instagram');
  $output .= unite_social_item(of_get_option('social_dribbble'), 'Dribbble', 'dribbble');
  $output .= unite_social_item(of_get_option('social_skype'), 'Skype', 'skype');
  $output .= unite_social_item(of_get_option('social_vimeo'), 'Vimeo', 'vimeo-square');
  $output .= '</div>';
  echo $output;
}

function unite_social_item($url, $title = '', $icon = ''){
  if($url != ''):
    $output = '<a class="social-profile" href="'.esc_url($url).'" target="_blank" title="'.$title.'">';
    if($icon != '') $output .= '<span class="social_icon fa fa-'.$icon.'"></span>';
    $output .= '</a>';
    return $output;
  endif;
}

// footer menu (should you choose to use one)
function unite_footer_links() {
        // display the WordPress Custom Menu if available
        wp_nav_menu(array(
                'container' => '',                              // remove nav container
                'container_class' => 'footer-links clearfix',   // class of container (should you choose to use it)
                'menu' => __( 'Footer Links', 'unite' ),        // nav name
                'menu_class' => 'nav footer-nav clearfix',      // adding custom nav class
                'theme_location' => 'footer-links',             // where it's located in the theme
                'before' => '',                                 // before the menu
                'after' => '',                                  // after the menu
                'link_before' => '',                            // before each link
                'link_after' => '',                             // after each link
                'depth' => 0,                                   // limit the depth of the nav
                'fallback_cb' => 'unite_footer_links_fallback'  // fallback function
        ));
} /* end unite footer link */

// Get Post Views - for Popular Posts widget
function unite_getPostViews($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 View";
    }
    return $count.' Views';
}
function unite_setPostViews($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}


add_action('unite_footer', 'unite_footer_info', 30);


function unite_footer_info() { ?>
   Powered by  <img src="<?php echo esc_url( get_stylesheet_directory_uri() ) ?>/assets/img/logo_white.png" width="120px" height="auto" alt="" />
   <?php
}


// Get theme options
if (!function_exists('get_unite_theme_options'))  {
    function get_unite_theme_options(){

      echo '<style type="text/css">';

      if ( of_get_option('link_color')) {
        echo 'a, #infinite-handle span {color:' . of_get_option('link_color') . '}';
      }
      if ( of_get_option('link_hover_color')) {
        echo 'a:hover {color: '.of_get_option('link_hover_color', '#000').';}';
      }
      if ( of_get_option('link_active_color')) {
        echo 'a:active {color: '.of_get_option('link_active_color', '#000').';}';
      }
      if ( of_get_option('element_color')) {
        echo '.btn-primary, .label-primary, .carousel-caption h4 {background-color: '.of_get_option('element_color', '#000').'; border-color: '.of_get_option('element_color', '#000').';} hr.section-divider:after, .entry-meta .fa { color: '.of_get_option('element_color', '#000').'}';
      }
      if ( of_get_option('element_color_hover')) {
        echo '.btn-primary:hover, .label-primary[href]:hover, .label-primary[href]:focus, #infinite-handle span:hover, .btn.btn-primary.read-more:hover, .btn-primary:hover, .btn-primary:focus, .btn-primary:active, .btn-primary.active, .site-main [class*="navigation"] a:hover, .more-link:hover, #image-navigation .nav-previous a:hover, #image-navigation .nav-next a:hover  { background-color: '.of_get_option('element_color_hover', '#000').'; border-color: '.of_get_option('element_color_hover', '#000').'; }';
      }
      if ( of_get_option('heading_color')) {
        echo 'h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6, .entry-title {color: '.of_get_option('heading_color', '#000').';}';
      }
      if ( of_get_option('top_nav_bg_color')) {
        echo '.navbar.navbar-default {background-color: '.of_get_option('top_nav_bg_color', '#000').';}';
      }
      if ( of_get_option('top_nav_link_color')) {
        echo '.navbar-default .navbar-nav > li > a, .navbar-default .navbar-nav > .open > a, .navbar-default .navbar-nav > .open > a:hover, .navbar-default .navbar-nav > .open > a:focus, .navbar-default .navbar-nav > .active > a, .navbar-default .navbar-nav > .active > a:hover, .navbar-default .navbar-nav > .active > a:focus { color: '.of_get_option('top_nav_link_color', '#000').';}';
      }
      if ( of_get_option('top_nav_dropdown_bg')) {
        echo '.dropdown-menu, .dropdown-menu > .active > a, .dropdown-menu > .active > a:hover, .dropdown-menu > .active > a:focus {background-color: '.of_get_option('top_nav_dropdown_bg', '#000').';}';
      }
      if ( of_get_option('top_nav_dropdown_item')) {
        echo '.navbar-default .navbar-nav .open .dropdown-menu > li > a { color: '.of_get_option('top_nav_dropdown_item', '#000').';}';
      }
      if ( of_get_option('footer_bg_color')) {
        echo '#colophon {background-color: '.of_get_option('footer_bg_color', '#000').';}';
      }
      if ( of_get_option('footer_text_color')) {
        echo '.copyright {color: '.of_get_option('footer_text_color', '#000').';}';
      }
      if ( of_get_option('footer_link_color')) {
        echo '.site-info a {color: '.of_get_option('footer_link_color', '#000').';}';
      }
      if ( of_get_option('social_color')) {
        echo '.social-profile {color: '.of_get_option('social_color', '#000').';}';
      }
      if ( of_get_option('social_hover_color')) {
        echo '.social-profile:hover {color: '.of_get_option('social_hover_color', '#000').';}';
      }
      $typography = of_get_option('main_body_typography');
      if ( $typography ) {
        echo '.entry-content {font-family: ' . $typography['face'] . '; font-size:' . $typography['size'] . '; font-weight: ' . $typography['style'] . '; color:'.$typography['color'] . ';}';
      }
      if ( of_get_option('custom_css')) {
        echo of_get_option('custom_css', 'no entry');
      }
        echo '</style>';
    }
}
add_action('wp_head','get_unite_theme_options', 10);