<?php

// INITIALIZE WIDGETS
function unite_widgets_init() {
	register_sidebar( array(
		'name'          => __('Main Sidebar', 'unite'),
		'id'            => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar(array(
    	'id' => 'footer1',
    	'name' => 'Footer 1',
    	'description' => 'Footer 1 Widget - Visible Everywhere',
    	'before_widget' => '<div id="%1$s" class="widget %2$s">',
    	'after_widget' => '</div>',
    	'before_title' => '<h3 class="widgettitle">',
    	'after_title' => '</h3>',
    ));
    register_sidebar(array(
      	'id' => 'footer2',
      	'name' => 'Footer 2',
      	'description' => 'Footer 2 Widget - Visible Everywhere',
      	'before_widget' => '<div id="%1$s" class="widget %2$s">',
      	'after_widget' => '</div>',
      	'before_title' => '<h3 class="widgettitle">',
      	'after_title' => '</h3>',
    ));
    register_sidebar(array(
      	'id' => 'footer3',
      	'name' => 'Footer 3',
      	'description' => 'Footer 3 Widget - Visible Everywhere',
      	'before_widget' => '<div id="%1$s" class="widget %2$s">',
      	'after_widget' => '</div>',
      	'before_title' => '<h3 class="widgettitle">',
      	'after_title' => '</h3>',
    ));
    register_sidebar(array(
      	'id' => 'footer4',
      	'name' => 'Footer 4',
      	'description' => 'Footer 4 Widget - Visible Everywhere',
      	'before_widget' => '<div id="%1$s" class="widget %2$s">',
      	'after_widget' => '</div>',
      	'before_title' => '<h3 class="widgettitle">',
      	'after_title' => '</h3>',
    ));
}
add_action('widgets_init', 'unite_widgets_init');