<?php
/*
 Template Name: 	Main User Page Constructor
 Description:		Costum Page template
 Modified: 			27/01/2015
 Author:			Arber Braja
*/
?>

<?php

$wpdb->hide_errors(); nocache_headers();
global $userdata; get_currentuserinfo();

if(!empty($_POST['action'])){
	require_once(ABSPATH . 'wp-admin/includes/user.php');
	check_admin_referer('update-profile_' . $user_ID);
	$errors = edit_user($user_ID);
	if (is_wp_error($errors)) { foreach($errors->get_error_messages() as $message) $errmsg = "$message"; }
	if($errmsg == '') {
		do_action('personal_options_update',$user_ID);
		$d_url = $_POST['dashboard_url'];
		wp_redirect(get_option("siteurl").'?page_id='.$post->ID.'&updated=true');
	}
	else {
		$errmsg = '<div class="box-red">' . $errmsg . '</div>';
		$errcolor = 'style="background-color:#FFEBE8;border:1px solid #CC0000;"';
	}
}

// Get user birthday
$birthday = esc_attr(get_the_author_meta('birthday', $userdata->ID));
$birthday_data = explode('/', $birthday);
if($birthday) {
	$bday = $birthday_data['0'];
	$bmonth = $birthday_data['1'];
	$byear = $birthday_data['2'];
}

$today = date('d/m/Y');
$today_parts = explode('/', $today);
$currDay = $today_parts['0'];
$currMonth = $today_parts['1'];
$currYear = $today_parts['2'];

$minYear = $currYear - 100;
$minAllowedYear = $currYear - 13;
$months = array('Gennaio', 'Febbraio', 'Marzo', 'Aprile', 'Maggio', 'Giugno', 'Luglio', 'Agosto', 'Settembre', 'Ottobre', 'Novembre','Dicembre');

get_currentuserinfo();
get_header(); ?>
	
	<div id="primary" class="content-area col-sm-12 col-md-12">
		<main id="main" class="site-main" role="main">

			<?php if(is_user_logged_in()) { ?>
				
				<form name="profile" action="" method="post" enctype="multipart/form-data">
					<?php wp_nonce_field('update-profile_' . $user_ID) ?>
					<input type="hidden" name="from" value="profile" />
					<input type="hidden" name="action" value="update" />
					<input type="hidden" name="checkuser_id" value="<?php echo $user_ID ?>" />
					<input type="hidden" name="dashboard_url" value="<?php echo get_option("dashboard_url"); ?>" />
					<input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id; ?>" />
						<?php if(isset($_GET['updated'])): $d_url = $_GET['d']; ?>
					  		<div class="alert alert-success">Profilo aggiornato con successo!</div>
						<?php elseif($errmsg != ""): ?>
					  		<div class="alert alert-danger"><?php echo $errmsg; ?></div>
						<?php endif;?>
					</table>

					<div class="row">
						<div class="col-md-12">
							<h2>Il mio account</h2>
						</div>
						<div class="col-md-12">
							<p class="bigger">Completa le informazioni del tuo profilo per avere accesso diretto alle offerte della tua Città e più vicine ai tuoi interessi.</p>
						</div>
					</div>
					<div class="row">
						<!-- First Name -->
						<div class="col-md-4 col-sm-12">
							<div class="form-group">
						  		<label for="first_name">Nome</label>
								<input class="form-control" type="text" name="first_name" id="first_name" value="<?php echo $userdata->first_name ?>" />
							</div>
						</div>
						<!-- Last Name -->
						<div class="col-md-4 col-sm-12">
							<div class="form-group">
						  		<label for="last_name">Cognome</label>
						  		<input class="form-control" type="text" name="last_name" id="last_name" value="<?php echo $userdata->last_name ?>" />
							</div>
						</div>
						<!-- Email -->
						<div class="col-md-4 col-sm-12">
							<div class="form-group">
							  	<label for="email">E-mail</label>
							  	<input class="form-control" type="text" name="email" id="email" value="<?php echo $userdata->user_email ?>" />
							</div>
						</div>
						<!-- Birthday -->
						<div class="col-md-4 col-sm-12">
							<div class="row">
								<div class="col-md-3">
									<div class="form-group">
									<label for="day">Giorno</label>
									<select class="form-control" id="day">
										<?php for($i=1; $i<= 31; $i++) :?>
										<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
										<?php endfor; ?>
									</select>
									</div>
								</div>
								<div class="col-md-5">
									<div class="form-group">
										<label for="month">Mese</label>
										<select class="form-control" id="month">
										<?php foreach($months as $key => $value) : ?>
											<option value="<?php echo $key+1; ?>"><?php echo $value; ?></option>
										<?php endforeach; ?>
										</select>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label for="year">Anno</label>
										<select class="form-control" id="year">
											<?php for($yWalker = $minAllowedYear; $yWalker >= $minYear; $yWalker--) : ?>
											<option value="<?php echo $yWalker; ?>"><?php echo $yWalker; ?></option>
											<?php endfor; ?>
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group hide-item">
							<label for="birthday">Data di nascita</label>
							<input class="form-control" type="text" name="birthday" id="birthday" value="<?php echo esc_attr(get_the_author_meta('birthday', $userdata->ID)); ?>" />
						</div>
						<script type="text/javascript">
							var $j = jQuery.noConflict();
							// Initialize values
							$j("#day").val("<?php echo $bday; ?>").prop("selected", true);
							$j("#month").val("<?php echo $bmonth; ?>").prop("selected", true);
							$j("#year").val("<?php echo $byear; ?>");
							// get values after change
							$j("#day, #month, #year").on('change', function() {
								var bday = $j("#day").val();
								var bmonth = $j("#month").val();
								var byear = $j("#year").val();
								$j("#birthday").val(bday + "/" + bmonth + "/" + byear);
								var birthday = $j("#birthday").val();
							});
						</script>
						<!-- Gender -->
						<div class="col-md-4 col-sm-12">
							<div class="form-group">
								<label for="gender">Sesso</label>
								<select class="form-control" name="gender" id="gender">
									<option value="">--- scegli sesso ----</option>
									<?php 
									$genders = array('male' => 'Uomo', 'female' => 'Donna');
									$actual_gender = esc_attr(get_the_author_meta('gender', $userdata->ID));
									foreach($genders as $key => $value) { ?>
										<option value="<?php echo $key; ?>" <?php if($key == $actual_gender) { echo "selected"; } ?>><?php echo $value; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<!-- Location -->
						<div class="col-md-4 col-sm-12">
							<div class="form-group">
								<label for="location">Provincia</label>
								<select class="form-control" name="location" id="location">
									<option value="">---- scegli provincia ---</option>
									<?php
									$actual_location = esc_attr(get_the_author_meta('location', $userdata->ID));
									$locations = getLocations();
									foreach($locations as $location) { ?>
										<option value="<?php echo $location; ?>" <?php if($location == $actual_location) { echo "selected"; } ?>><?php echo $location; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<!-- New Password -->
						<div class="col-md-4 col-sm-12">
							<div class="form-group">
							  	<label for="pass1">Nuovo password</label>
							  	<input class="form-control" type="password" name="pass1" id="pass1" value="" />
							</div>
						</div>
						<!-- Confirm New Password -->
						<div class="col-md-4 col-sm-12">
							<div class="form-group">
							  	<label for="pass2">Conferma nuovo password</label>
							  	<input class="form-control"  type="password" name="pass2" id="pass2" value="" />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 pull-right">
							<div class="form-group">
								<input type="submit" class="btn btn-success" value="Aggiorna" />
								<?php $redirect = home_url(); ?>
								<a href="<?php echo wp_logout_url($redirect); ?>" class="btn btn-danger logout">Esci</a>
							  	<input type="hidden" name="action" value="update" />
							</div>
						</div>
					</div>
				</form>
				
			<?php } else if(!is_user_logged_in()) { ?>

				<div class="row">
					<div class="col-md-12">
						<h3>Registrati/Login</h3>
					</div>
				</div>
				<div class="row login-register">
					<div class="col-md-12">
						<ul class="nav toggle-content" id="accountTab">
				  			<li class=""><a data-toggle="tab" href="#login">Login</a></li>
				  			<li class="active"><a data-toggle="tab" href="#register">Registrati</a></li>	
						</ul>
						<div class="tab-content">
				  			<div id="login" class="tab-pane">
				    			<?php echo do_shortcode('[login_form]'); ?>
				  			</div>
						    <div id="register" class="tab-pane active">
						    	<?php echo do_shortcode('[register_form]'); ?>
						    </div>
						</div>
					</div>
				</div>

			<?php } ?>

		</main>
	</div>

<?php get_footer(); ?>
