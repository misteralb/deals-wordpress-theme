<?php
/*
 Template Name:   Contact Page
 Description:   Costum Page template
 Modified:      27/01/2015
 Author:      Arber Braja
*/
?>

<?php

$response = "";

function my_contact_form_generate_response($type, $message){
    global $response;
    if($type == "success") $response = "<div class='success messages'>{$message}</div>";
    else $response = "<div class='error messages'>{$message}</div>";
}

$email_invalid   = "Indirizzo email non valido";
$message_unsent  = "Il messagio non e stato mandatto con successo.";
$message_sent    = "Grazie! Il vostro messaggio e stato mandatto con successo.";

$name = $_POST['message_name'];
$surname = $_POST['message_surname'];
$email = $_POST['message_email'];
$message = $_POST['message_text'];

$to = get_option('admin_email');
$subject = "Nuovo messaggio email: " . get_bloginfo('name');
$headers = 'From: '. $email . "\r\n" . 'Reply-To: ' . $email . "\r\n";

$sent = wp_mail($to, $subject, strip_tags($message), $headers);
if($sent) { my_contact_form_generate_response("success", $message_sent); }

?>

<?php get_header(); ?>

  <div id="primary" class="content-area col-sm-12 col-md-8">
      <main id="main" class="site-main" role="main">

          <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
              <header class="entry-header">
                  <h1 class="entry-title"><?php the_title(); ?></h1>
              </header>

              <div class="entry-content">

                  <style type="text/css">
                      #respond { font-family: Arial; font-size: 16px; }
                      .messages { padding: 5px 9px; border-radius: 3px; margin-bottom: 15px; }
                      .error { border: 1px solid red; color: red; }
                      .success { border: 1px solid green; color: green; }
                  </style>

                  <div id="respond">
                      <?php echo $response; ?>
                      <form action="<?php the_permalink(); ?>" method="post">
                          <div class="row">
                              <div class="col-md-6 col-sm-12">
                                  <div class="form-group">
                                      <label for="message_name">Nome: *</label>
                                      <input required class="form-control" type="text" name="message_name" id="message_name" value="<?php echo esc_attr($_POST['message_name']); ?>"/>
                                  </div>
                              </div>
                              <div class="col-md-6 col-sm-12">
                                  <div class="form-group">
                                      <label for="message_surname">Cognome: </label>
                                      <input class="form-control" type="text" name="message_surname" id="message_surname" value="<?php echo esc_attr($_POST['message_surname']); ?>"/>
                                  </div>
                              </div>
                              <div class="col-md-12">
                                  <div class="form-group">
                                      <label for="message_email">Email: </label>
                                      <input required class="form-control" type="email" name="message_email" id="message_email" value="<?php echo esc_attr($_POST['message_email']); ?>"/>
                                  </div>
                              </div>
                              <div class="col-md-12">
                                  <div class="form-group">
                                      <label for="message_text">Messaggio: *</label>
                                      <textarea required class="form-control" rows="5" name="message_text" id="message_text"></textarea>
                                  </div>
                              </div>
                          </div>
                          <input type="hidden" name="submitted" value="1">
                          <p><input class="btn btn-success" type="submit" value="Invia"></p>
                      </form>
                  </div>

              </div><!-- .entry-content -->
          </article><!-- #post -->

    </main>
  </div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>
