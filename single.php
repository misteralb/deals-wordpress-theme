<?php
/*
 Template: 	Single Page/Post Constructor
 Modified: 	26/01/2015
 Author:	Arber Braja
*/
?>

<?php get_header(); ?>
	
	<?php $post_class = get_post_class(); ?>

	<div id="primary" class="content-area col-sm-12 <?php if($post_class[1] != "deals") { echo "col-md-8 "; } else { echo "col-md-9 "; } ?>">
		<main id="main" class="site-main" role="main">

		<?php
		while(have_posts()) : the_post();
			if(get_post_type() == "deals") {
				get_template_part('content', 'deals');
			} else {
				get_template_part('content', 'single');
			}
			unite_post_nav();
		endwhile;
		?>

		</main>
	</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>