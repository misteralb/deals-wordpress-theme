<?php 

if(wp_is_android() && !isset($_COOKIE['mobile']) && $_SERVER['HTTP_X_REQUESTED_WITH'] != "com.arbraja.omaggiweb") {
	header('Location: http://www.omaggiweb.it/apps/');
}

?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<!-- TradeDoubler site verification 2480254 -->
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="verification" content="7701f6f777a5332cb1481e9e514522d2" />
	<title><?php wp_title('|', true, 'right'); ?></title>
	<?php wp_head(); ?>
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	 	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	 	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	  	ga('create', 'UA-42712269-1', 'auto');
	  	ga('send', 'pageview');
	</script>
</head>

<body <?php body_class(); ?>>

<div id="page" class="site">

	<div id="closed"></div>
	<div class="popup-wrapper" id="popup">
		<div class="popup-container">
			<?php echo do_shortcode('[login_form]'); ?>
			<a class="popup-close" href="#closed">X</a>
		</div>
	</div>

	<div id="topHeader">
		<div class="container">
			<div class="searchform"><?php get_search_form(); ?></div>
			<div class="socials"><?php unite_social(); ?></div>
			<div class="account"><?php header_login_form(); ?></div>
		</div>
	</div>

	<div class="container">
		<?php do_action('before'); ?>
		<header id="masthead" class="site-header" role="banner">

				<div class="hbox site-branding">
					<h1 class="site-title">
						<a href="<?php echo home_url('/'); ?>" title="<?php bloginfo('name'); ?>"><img src="<?php echo esc_url(get_stylesheet_directory_uri()) ?>/assets/img/logo_blue.png" alt="" /></a>
					</h1>
				</div>

				<div class="hbox header-middle">
					<?php new_city_choser(); ?>
				</div>

				<div class="hbox fbbutton">
					<div class="fb-follow" data-href="https://www.facebook.com/OmaggiWeb" data-width="350" data-height="100" data-colorscheme="light" data-layout="standard" data-show-faces="true"></div>
				</div>

		</header>
	</div>
		<nav class="navbar navbar-default" role="navigation">
			<div class="container">
		        <div class="navbar-header">
		            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
		                <span class="sr-only">Toggle navigation</span>
		                <span class="icon-bar"></span>
		                <span class="icon-bar"></span>
		                <span class="icon-bar"></span>
		            </button>
		        </div>

				<?php
		            wp_nav_menu(array(
		                'theme_location'    => 'primary',
		                'depth'             => 3,
		                'container'         => '',
		                'container_class'   => 'collapse navbar-collapse navbar-ex1-collapse',
		                'menu_class'        => 'nav navbar-nav',
		                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
		                'walker'            => new wp_bootstrap_navwalker())
		            );
		        ?>
		    </div>
		</nav>

		<?php
		if(is_front_page() || is_tax('deals_city')) { deals_homepage_slider(); }
		else { if(is_tax('deals_category')) { deals_category_slider(); } }
		?>

	<div id="content" class="site-content container">