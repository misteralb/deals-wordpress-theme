<?php
/*
 Template:	Category Deals Listing
 Modified:	26/01/2015
 Author:	Arber Braja
*/
 ?>

<?php get_header(); ?>

	<div id="primary" class="content-area col-sm-12 col-md-12">
		<main id="main" class="site-main" role="main">

			<?php

			$category_name = get_queried_object()->name;
			$city = $_COOKIE['locality'];
			if(!isset($city)) { $city = "Italia"; }

			// Personalised Experience Variables
    		$userinfo = userDetails();
    		$user_city = $userinfo->location;
    		$user_gender = $userinfo->gender;
    		$user_birthday = $userinfo->birthday;

    		echo do_shortcode('[featured_deals]');
    		
    		// city AND user_city have values --- show city deals
    		if(isset($city) AND isset($user_city)) { $city = $_COOKIE['locality']; }
    		// only user_city has value --- show user_city deals
			else if(isset($user_city) AND !isset($city)) { $city = $user_city; }
			// only city has value --- show city deals
			else if(isset($city) AND !isset($user_city)) { $city = $_COOKIE['locality']; }
			// city AND user_city have no value --- show national deals
			else if(!isset($city) AND !isset($user_city)) { $city = "Italia"; }

			if($category_name == "eBook" || $category_name == "Kobo" || $category_name == "PDF" || $category_name == "Kindle") {
				$args = array(
					'post_type' => 'deals',
					'posts_per_page' => 16,
					'deals_city' => 'Italia',
					'deals_category' => $category_name,
					'paged' => $paged,
					'meta_query' => array(
                		array(
                			'key' => 'gender',
                			'value' => array($user_gender, 'Entrambi'),
                			'compare' => 'IN'
                    	)
            		)
				);
			} else {
				$args = array(
					'post_type' => 'deals',
					'posts_per_page' => 16,
					'deals_city' => $city,
					'deals_category' => $category_name,
					'paged' => $paged,
					'meta_query' => array(
                		array(
                			'key' => 'gender',
                			'value' => array($user_gender, 'Entrambi'),
                			'compare' => 'IN'
                    	)
            		)
				);
			}
			

		    $deals = new WP_Query($args);
		    if($deals->have_posts()) : ?>

	        	<div class="row dealtitle">
			        <div class="col-md-12">
			            <h2 class="pull-left">
		                    <?php
		                    // city and user_city have values
		                    if(isset($user_city) AND isset($city) AND $user_city != "" AND $city != "") {
		                        if($city == "Italia") { echo "Offerte nazionali a " . $category_name; }
		                        else { echo $category_name . " a " . $city; }
		                    }
		                    // only city has value
		                    else if(!isset($user_city) AND isset($city) AND $city != "") {
		                        if($city == "Italia") { echo "Offerte nazionali a " . $category_name; }
		                        else { echo $category_name . " a " . $city; }
		                    }
		                    // only user_city has value
		                    else if(isset($user_city) AND !isset($city) AND $user_city != "") {
		                        if($city == "Italia") { echo "Offerte nazionali a " . $category_name; }
		                        else { echo $category_name . ' a ' . $user_city; }
		                    }
		                    // city and user_city have no value
		                    else { echo $category_name; } ?>
		                </h2>
			        </div>
			    </div>

			    <div class="row deals">
				    <?php
				    while($deals->have_posts()) : $deals->the_post();
				        $deal_image = get_field('deal_image');
				        $deal_link = get_permalink();
				        $deal_end_date = get_field('deal_end_date');
				    ?>
				    <div class="col-md-3 dealpanel">
				        <div class="dealimg">
				            <a href="<?php echo $deal_link; ?>" title="<?php echo the_title(); ?>">
				            	<?php deal_image(); ?>
				            </a>
				        </div>
				        <div class="dealdetails">
				            <h4><a href="<?php echo $deal_link; ?>" title=""><?php echo the_title(); ?></a></h4>
				            <span class="expire-date"><em>Disponibile fino al:</em> <b><?php echo $deal_end_date; ?></b></span>
				            <span class="buynow"><a href="<?php echo $deal_link; ?>" title="">Visualizza Offerta</a></span>
				        </div>
				    </div>
				    <?php
				    endwhile;
				    ?>
			    </div>

				<?php
			else : ?>

				<div class="row dealtitle">
		            <div class="col-md-12">
		                <h2 class="pull-left">
		                    <?php
		                    // city and user_city have values
		                    if(isset($user_city) AND isset($city) AND $user_city != "" AND $city != "") {
		                        if($city == "Italia") { echo "Offerte nazionali a " . $category_name; }
		                        else { echo $category_name . " a " . $city; }
		                    }
		                    // only city has value
		                    else if(!isset($user_city) AND isset($city) AND $city != "") {
		                        if($city == "Italia") { echo "Offerte nazionali a " . $category_name; }
		                        else { echo $category_name . " a " . $city; }
		                    }
		                    // only user_city has value
		                    else if(isset($user_city) AND !isset($city) AND $user_city != "") {
		                        if($city == "Italia") { echo "Offerte nazionali a " . $category_name; }
		                        else { echo $category_name . ' a ' . $user_city; }
		                    }
		                    // city and user_city have no value
		                    else { echo $category_name; } ?>
		                </h2>
		            </div>
		        </div>

		        <div class="row deals">
		            <div class="col-md-12">
		                <p class="margin-top-10 nodeals"><big>Nessuna offerta disponibile, guarda tra le offerte <span id="show-nationals">Nazionali</span></big></p>
		            </div>
		        </div>

				<?php
			endif;
			?>

		</main>

		<?php wp_reset_query(); ?>

		<div class="navigation-links">
			<div class="navigation">
				<?php if(get_previous_posts_link()) : ?>
				<div class="alignleft"><?php previous_posts_link('&laquo; Precedente') ?></div>
				<?php endif; ?>
				<?php if(get_next_posts_link('', $deals->max_num_pages)) : ?>
				<div class="alignright"><?php next_posts_link('Successivo &raquo;') ?></div>
				<?php endif; ?>
			</div>
		</div>

	</div>

<?php get_footer(); ?>
