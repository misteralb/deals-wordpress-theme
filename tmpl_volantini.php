<?php
/*
 Template Name: 	Volantini Page
 Description:		Costum Page template
 Modified: 			27/01/2015
 Author:			Arber Braja
*/
?>

<?php

// Personalised Experience Variables
$userinfo = userDetails();
$user_city = $userinfo->location;
$city = $_COOKIE['locality'];
$city_value = sliderCityChoser($city, $user_city);

// Filtering
$category_filter = $_GET['category'];

if($city_value == "Italia") { $city_value = "Roma"; }

$volCategories = array('Supermercati', 'Casa', 'Moda', 'Elettronica', 'Profumerie', 'Giardino e Brico', 'Sport', 'Giochi e neonati', 'Libri', 'Viaggi');

?>

<?php get_header(); ?>

	<div id="primary" class="content-area col-sm-12 col-md-12">
		<main id="main" class="site-main" role="main">

			<!-- Volantini Filtering System -->
			<div class="row">
				<form class="" method="GET" action="">
					<div class="col-md-4 form-group hide-item">
						<select class="form-control" id="city">
							<option value="">--- chose city ---</option>
							<?php 
							$locations = getLocations();
							foreach($locations as $location) { ?>
								<option value="<?php echo $location; ?>" <?php if($location == $city_filter) { echo 'selected="selected"'; } ?>><?php echo $location; ?></option>
							<?php } ?>
						</select>
					</div>
					<div class="col-md-4 form-group">
						<select class="form-control" id="category" name="category">
							<option value="">--- chose category ---</option>
							<?php 
							foreach($volCategories as $category) { ?>
								<option value="<?php echo $category; ?>" <?php if($category == $category_filter) { echo 'selected="selected"'; } ?>><?php echo $category; ?></option>
							<?php } ?>
						</select>
					</div>
					<div class="col-md-2 form-group">
						<input class="form-control btn btn-success" type="submit" value="Filtra i risultati" />
					</div>
				</form>
			</div>

			<!-- Show Volantini -->
			<div class="row volantini-default">
				<div class="col-md-12">
					<?php echo '<iframe width="100%" height="270" frameborder="0" src="http://www.tiendeo.it/widget/visor1.aspx?origen=omaggiweb&ciudad=' . $city_value . '&buscar='. $category_filter . '" name="volantini"></iframe>'; ?>
				</div>
			</div>

		</main>
	</div>

<?php get_footer(); ?>
