<?php
/*
 Template:	Search Form
 Modified:	26/01/2015
 Author:	Arber Braja
*/
?>

<form role="search" method="get" class="search-form form-inline" action="<?php echo esc_url(home_url('/')); ?>">
  	<label class="sr-only"><?php _e('Cercate per:', 'wpdeals'); ?></label>
  	<div class="input-group">
    	<input type="search" value="<?php echo get_search_query(); ?>" name="s" class="search-field form-control" placeholder="<?php _e('Cerca...', 'wpdeals'); ?>">
    	<span class="input-group-btn">
      		<button type="submit" class="search-submit btn btn-primary"><span class="glyphicon glyphicon-search"></span></button>
    	</span>
  	</div>
</form>
