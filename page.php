<?php
/*
 Template:	Page (with sidebar)
 Modified:	26/01/2015
 Author:	Arber Braja
*/
?>

<?php get_header(); ?>

	<div id="primary" class="content-area col-sm-12 col-md-8">
		<main id="main" class="site-main" role="main">

			<?php
			while(have_posts()) : the_post();
				get_template_part('content', 'page');
			endwhile;
			?>

		</main>
	</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>
