<?php
/*
 Template:	Footer
 Modified: 	27/01/2015
 Author:	Arber Braja
*/
?>
			</div>

			<div class="footer-widgets">
				<div class="container">
					<div class="col-xs-12 col-sm-3 col-md-3 home-widget">
						<?php if(is_active_sidebar('footer1')) dynamic_sidebar('footer1'); ?>
					</div>
					<div class="col-xs-12 col-sm-3 col-md-3 home-widget">
						<?php if(is_active_sidebar('footer2')) dynamic_sidebar('footer2'); ?>
					</div>
					<div class="col-xs-12 col-sm-3 col-md-3 home-widget">
						<?php if(is_active_sidebar('footer3')) dynamic_sidebar('footer3'); ?>
					</div>
					<div class="col-xs-12 col-sm-3 col-md-3 home-widget">
						<?php if(is_active_sidebar('footer4')) dynamic_sidebar('footer4'); ?>
					</div>
				</div>
			</div>

			<footer id="colophon" class="site-footer" role="contentinfo">
				<div class="site-info container">
					<div class="row">
						<nav role="navigation" class="col-md-6">
							<?php unite_footer_links(); ?>
						</nav>
						<div class="copyright col-md-6">
							<?php do_action('unite_credits'); ?>
							<?php echo of_get_option('custom_footer_text', 'wpdeals'); ?>
							<?php do_action('unite_footer'); ?>
						</div>
					</div>
				</div>
			</footer>
		</div>

		<?php wp_footer(); ?>
		
		<script type="text/javascript">
			var $j = jQuery.noConflict();
		   	$j(function(){ $j('select#city-choser').selectToAutocomplete(); });
		   	$j('#birthday').datepicker({ format: "dd/mm/yyyy" });
		   	$j('#pippin_birthday').datepicker({ format: "dd/mm/yyyy" });

			function autoResize(id){
	   			var newheight;
	    			var newwidth;

	    			if(document.getElementById){
	        		newheight=document.getElementById(id).contentWindow.document.body.scrollHeight;
	        		newwidth=document.getElementById(id).contentWindow.document.body.scrollWidth;
	    			}

	    			document.getElementById(id).height=(newheight) + "px";
	    			document.getElementById(id).width=(newwidth) + "px";
			}
		</script>

		<script type="text/javascript">
			 $j("li.omaggio a:first").prepend('<i class="ab-omaggio"></i>  ');
			 $j("li.sconti a:first").prepend('<i class="ab-sconti"></i>  ');
			 $j("li.coupon a:first").prepend('<i class="ab-coupon"></i>  ');
			 $j("li.concorsi a:first").prepend('<i class="ab-sondaggi"></i>  ');
			 $j("li.ebook a:first").prepend('<i class="ab-ebook"></i>  ');
			 $j("li.compara a:first").prepend('<i class="ab-compara"></i>  ');
			 $j("li.volantini a:first").prepend('<i class="ab-volantini"></i>  ');
			 $j("li.bonus a:first").prepend('<i class="ab-currency"></i>  ');
			 $j("li.travel a:first").prepend('<i class="ab-travel"></i>  ');
			 $j(".fball_ui a:first").append('  <i class="fa fa-facebook"></i>');
		</script>

	</body>
</html>