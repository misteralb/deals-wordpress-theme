<?php
/*
 Template:	Search Results
 Modified:	26/01/2015
 Author:	Arber Braja
*/
?>

<?php get_header(); ?>

	<section id="primary" class="content-area col-sm-12 col-md-12">
		<main id="main" class="site-main" role="main">

		<?php 
		if (have_posts()) :
		?>

			<header class="col-md-12 page-header">
				<h1 class="page-title"><?php printf(__('Risultati di ricerca per: %s', 'wpdeals'), '<span>' . get_search_query() . '</span>'); ?></h1>
			</header>

			<?php 
				while(have_posts()) : the_post();
					get_template_part('content', 'search');
				endwhile;
				unite_paging_nav();
			else :
				get_template_part('content', 'none');
		endif;
		?>

		</main>

		<div class="col-md-12 navigation-links">
			<div class="navigation">
				<?php if(get_previous_posts_link()) : ?>
				<div class="alignleft"><?php previous_posts_link('&laquo; Previous') ?></div>
				<?php endif; ?>
				<?php if(get_next_posts_link()) : ?>
				<div class="alignright"><?php next_posts_link('Next &raquo;') ?></div>
				<?php endif; ?>
			</div>
		</div>
			
	</section>

<?php get_footer(); ?>
