<?php
/*
 Template:	Posts Listing (Blog Page)
 Modified:	26/01/2015
 Author:	Arber Braja
*/
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="col-md-12">
		<h2><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
	</header>

	<div class="row entry-content">
		<div class="col-sm-12 col-md-3">
			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
				<img class="blogimg" src="<?php echo get_first_image(); ?>" alt="<?php the_title(); ?>" />
			</a>
		</div>
		<div class="col-sm-12 col-md-9 blog-excerpt">
			<?php the_excerpt(); ?>
		</div>
	</div>
</article>