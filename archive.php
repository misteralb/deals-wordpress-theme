<?php
/*
 Template: 	Archive
 Modified: 	26/01/2015
 Author:	Arber Braja
*/
?>

<?php get_header(); ?>
	
	<?php
	$post_type = get_post_type();
	if($post_type == "post") {
	?>

		<section id="primary" class="content-area col-sm-12 col-md-8">
			<main id="main" class="site-main" role="main">

				<?php
					if(have_posts()) : ?>
						<header class="col-md-12 page-header">
							<h1 class="page-title"><?php _e('Tutte le offerte', 'wpdeals'); ?></h1>
						</header>
						<?php
						while(have_posts()) : the_post();
							get_template_part('content', get_post_format());
						endwhile;
						unite_paging_nav();
					else :
						get_template_part('content', 'none');
					endif;
				?>

			</main>

			<div class="col-md-12 navigation-links">
				<div class="navigation">
					<?php if(get_previous_posts_link()) : ?>
					<div class="alignleft"><?php previous_posts_link('&laquo; Precedente') ?></div>
					<?php endif; ?>
					<?php if(get_next_posts_link()) : ?>
					<div class="alignright"><?php next_posts_link('Successivo &raquo;') ?></div>
					<?php endif; ?>
				</div>
			</div>

		</section>

	<?php } else { ?>

		<section id="primary" class="content-area col-sm-12 col-md-12">
			<main id="main" class="site-main" role="main">

			<?php
				if(have_posts()) : ?>
					<header class="col-md-12 page-header">
						<h1 class="page-title"><?php _e('Tutte le offerte', 'wpdeals'); ?></h1>
					</header>
					<?php
					while(have_posts()) : the_post();
						get_template_part('content', get_post_format());
					endwhile;
					unite_paging_nav();
				else :
					get_template_part('content', 'none');
				endif;
			?>

			</main>

			<div class="col-md-12 navigation-links">
				<div class="navigation">
					<?php if(get_previous_posts_link()) : ?>
					<div class="alignleft"><?php previous_posts_link('&laquo; Precedente') ?></div>
					<?php endif; ?>
					<?php if(get_next_posts_link()) : ?>
					<div class="alignright"><?php next_posts_link('Successivo &raquo;') ?></div>
					<?php endif; ?>
				</div>
			</div>

		</section>

	<?php } ?>

<?php if($post_type == "post") { get_sidebar(); } ?>
<?php get_footer(); ?>
