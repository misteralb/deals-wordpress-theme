<?php

// Add new columns on the All Deals view
add_filter('manage_edit-deals_columns', 'my_edit_deals_columns');
function my_edit_deals_columns($columns) {
	$columns = array(
		'cb' 			=> '<input type="checkbox" />',
		'deal_id' 		=> __('ID', 'wpdeals'),
		'title' 		=> __('Deal Title', 'wpdeals'),
		'expire' 		=> __('Expiration Date', 'wpdeals'),
		'frontend_deal' => __('Frontend Deal', 'wpdeals'),
		'deal_status' 	=> __('Deal Status', 'wpdeals'),
		'deal_category' => __('Category', 'wpdeals'),
		'deal_location' => __('Deal Location', 'wpdeals'),
		'date' 			=> __('Date', 'wpdeals')
	);
	return $columns;
}

// Add values to the new added columns
add_action('manage_deals_posts_custom_column', 'deals_editscreen_columns', 10, 2);
function deals_editscreen_columns($column, $post_id) {
	global $post;

	switch($column) {
		case 'deal_id' :
			echo $post_id;
			break;
		case 'expire' :
			$expiredate = the_field('deal_end_date');
			echo $expiredate;
			break;
		case 'frontend_deal' :
			$frontend_deal = get_field('frontend_deal');
			if($frontend_deal == '1') { echo "Frontend"; }
			break;
		case 'deal_status' :
			$deal_status = the_field('deal_status');
			echo $deal_status;
			break;
		case 'deal_category' :
			$categories = get_the_terms($post->ID, 'deals_category');
			foreach ($categories as $category) {
				echo $category->name . "<br/>";
			}
			break;
		case 'deal_location' :
			$locations = get_the_terms($post->ID, 'deals_city');
			if($locations != NULL) {
				foreach ($locations as $location) {
					echo $location->name . "<br/>";
				}
			}
			break;
		default :
			break;
	}
}

// Make costum columns sortable
add_filter('manage_edit-deals_sortable_columns', 'deals_sortable_columns');
function deals_sortable_columns($columns) {
	$columns['deal_id'] 		= 'deal_id';
	$columns['expire'] 			= 'expire';
	$columns['frontend_deal'] 	= 'frontend_deal';
	$columns['deal_status'] 	= 'deal_status';
	$columns['deal_category'] 	= 'deal_category';
	$columns['deal_location'] 	= 'deal_location';
	return $columns;
}