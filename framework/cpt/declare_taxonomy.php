<?php

// START Categories for the Deals CPT
add_action('init', 'deal_categories_taxonomy', 0);
function deal_categories_taxonomy() {
  	$labels = array(
	    'name' 				=> _x('Deal Categories', 'Categories of the deals', 'wpdeals'),
	    'singular_name' 	=> _x('Category', 'Categories of the deals', 'wpdeals'),
	    'search_items' 		=>  __('Search Deal Categories', 'wpdeals'),
	    'all_items' 		=> __('All Deal Categories', 'wpdeals'),
	    'parent_item' 		=> __('Parent Deal Category', 'wpdeals'),
	    'parent_item_colon' => __('Parent Deal Category:', 'wpdeals'),
	    'edit_item' 		=> __('Edit Deal Category', 'wpdeals'), 
	    'update_item' 		=> __('Update Deal Category', 'wpdeals'),
	    'add_new_item' 		=> __('Add New Deal Category', 'wpdeals'),
	    'new_item_name' 	=> __('New Deal Category', 'wpdeals'),
	    'menu_name' 		=> __('Deal Categories', 'wpdeals'),
  	); 	
	// Now register the taxonomy
	register_taxonomy('deals_category', array('deals'), array(
	    'hierarchical' 		=> true,
	    'labels' 			=> $labels,
	    'show_ui' 			=> true,
	    'show_admin_column' => true,
	    'query_var' 		=> true,
	    'rewrite' 			=> array('slug'=>'deals_category'),
  	));
} // END Categories for the Deals CPT

// START Locations for the Deals CPT
add_action('init', 'deal_places_taxonomy', 0);
function deal_places_taxonomy() {
  	$labels = array(
	    'name' 				=> _x('Deal Locations', 'Locations where the deals are available', 'wpdeals'),
	    'singular_name' 	=> _x('Deal Location', 'Place of the deal', 'wpdeals'),
	    'search_items' 		=>  __('Search Deal Locations', 'wpdeals'),
	    'all_items' 		=> __('All Deal Locations', 'wpdeals'),
	    'parent_item' 		=> __('Parent Deal Locations', 'wpdeals'),
	    'parent_item_colon' => __('Parent Deal Locations:', 'wpdeals'),
	    'edit_item' 		=> __('Edit Deal Location', 'wpdeals'), 
	    'update_item' 		=> __('Update Deal Location', 'wpdeals'),
	    'add_new_item' 		=> __('Add New Deal Location', 'wpdeals'),
	    'new_item_name' 	=> __('New Deal Location', 'wpdeals'),
	    'menu_name' 		=> __('Deal Locations', 'wpdeals'),
  	); 	
	// Now register the taxonomy
	register_taxonomy('deals_city', array('deals'), array(
	    'hierarchical' => true,
	    'labels' => $labels,
	    'show_ui' => true,
	    'show_admin_column' => true,
	    'query_var' => true,
	    'rewrite' => array('slug'=>'deals_city'),
  	));
} // START Locations for the Deals CPT