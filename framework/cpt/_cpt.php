<?php

// DECLARE COSTUM POST TYPES
require 'declare_cpt.php';

// DECLARE CPT TAXONOMIES
require 'declare_taxonomy.php';

// HELPERS FOR CPT
require 'cpt_helpers.php';
