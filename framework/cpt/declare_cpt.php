<?php

// START Deals CPT Declaration
if (!function_exists('deal_post_type')) {
	// Register Custom Post Type
	function deal_post_type() {
		$labels = array(
			'name'                => _x('Deals', 'Deals', 'deals'),
			'singular_name'       => _x('Deal', 'Deal', 'deals'),
			'menu_name'           => __('Deals', 'deals'),
			'parent_item_colon'   => __('Parent Deal:', 'deals'),
			'all_items'           => __('All Deals', 'deals'),
			'view_item'           => __('View Deal', 'deals'),
			'add_new_item'        => __('Add New Deal', 'deals'),
			'add_new'             => __('Add New', 'deals'),
			'edit_item'           => __('Edit Deal', 'deals'),
			'update_item'         => __('Update Deal', 'deals'),
			'search_items'        => __('Search Deals', 'deals'),
			'not_found'           => __('Not found', 'deals'),
			'not_found_in_trash'  => __('Not found in Trash', 'deals')
		);
		$args = array(
			'label'               => __('deals', 'deals'),
			'description'         => __('Deals', 'deals'),
			'labels'              => $labels,
			'supports'			  => array('title', 'editor', 'comments', 'thumbnail', 'custom-fields'),
			'taxonomies'          => array('deals_category', 'deals_city'),
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => 2,
			'menu_icon'           => 'dashicons-products',
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'capability_type'     => 'page'
		);
		register_post_type('deals', $args);
	}
	// Hook into the 'init' action
	add_action('init', 'deal_post_type', 0);
} // END Deals CPT Declaration
