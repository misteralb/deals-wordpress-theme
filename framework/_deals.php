<?php

// HELPER FUNCTIONS
require dirname( __FILE__ ) . '/_helpers/_helpers.php';

// COSTUM POST TYPE
require dirname( __FILE__ ) . '/cpt/_cpt.php';

// USERS
require dirname( __FILE__ ) . '/users/_users.php';

// SHORTCODES
require dirname( __FILE__ ) . '/shortcodes/_shortcodes.php';

// AUTOMATIC DEALS
require dirname( __FILE__ ) . '/automatic-deals/_automatic-deals.php';