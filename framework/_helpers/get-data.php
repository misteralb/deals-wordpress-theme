<?php

// Deal Locations
function getLocations() {
  	$args = array(
      'orderby'           => 'name', 
      'order'             => 'ASC',
      'hide_empty'        => true, 
      'number'            => '', 
      'fields'            => 'names',
  	);
  	$locations = get_terms('deals_city', $args);
  	return $locations;
}

function getFullLocations() {
    $args = array(
      'orderby'           => 'name', 
      'order'             => 'ASC',
      'hide_empty'        => false, 
      'number'            => ''
    );
    $categories = get_terms('deals_city', $args);
    return $categories;
}


// Deal Categories
function getCategories() {
	$args = array(
      'orderby'           => 'name', 
      'order'             => 'ASC',
      'hide_empty'        => false, 
      'number'            => '', 
      'fields'            => 'names',
  	);
  	$categories = get_terms('deals_category', $args);
  	return $categories;
}

function getParentCategories() {
    $args = array(
      'orderby' => 'name',
      'parent' => 0
    );
    $categories = get_terms('deals_category', $args);
    return $categories;
}

// Deal Categories Full Details
function getFullCategories() {
    $args = array(
      'orderby'           => 'name', 
      'order'             => 'ASC',
      'hide_empty'        => false, 
      'number'            => ''
    );
    $categories = get_terms('deals_category', $args);
    return $categories;
}

// Deals
function all_deals() {
	$city = $_GET['city'];
	$category = $_GET['category'];
	$posts = $_GET['posts'];
	$sortby = $_GET['sortby'];

	$args = array(
      'post_type'     	 => 'deals',
    	'deals_city'		   => $city,
    	'deals_category' 	 => $category,
    	'posts_per_page'	 => $posts,
    	'order'				     => 'DESC',
    	'orderby'			     => 'date',
    );
	$deals = new WP_Query($args);
	return $deals;
}