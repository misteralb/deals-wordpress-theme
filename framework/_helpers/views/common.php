<?php

// START Deals Sorter
function deals_sorter() { ?>    
    <div class="sorter pull-right">
        <form method="GET" action ="">
            <span>Ordina per </span>
            <select id="sorter" onchange="top.location.href=this.options[this.selectedIndex].value;" class="sortby">
                <option value="default" selected="selected" disabled="disabled">-- default --</option>
                <option value="?sortby=recent">Più recenti</option>
                <option value="?sortby=popular">Più popolari</option>
            </select>
        </form>
    </div>
    <?php
} // END Deals Sorter


// Subscribe Newsletter Horizontal Layout
function subscribe_newsletter_horizontal() {
	$current_user = wp_get_current_user(); ?>
	
	<script type="text/javascript">
		if (typeof newsletter_check !== "function") {
			window.newsletter_check = function (f) {
		    	var re = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-]{1,})+\.)+([a-zA-Z0-9]{2,})+$/;
		    	if (!re.test(f.elements["ne"].value)) {
		        	alert("L'indirizzo email entrato non e corretto!");
		        	return false;
		    	}
		    	if (f.elements["ny"] && !f.elements["ny"].checked) {
		        	alert("Dovrete accettare le regole per il trattamento dei vostri dati!");
		        	return false;
		    	}
		    	return true;
			}
		}
	</script>

	<div class="row subscribe">
		<form method="post" action="http://www.omaggiweb.it/wp-content/plugins/newsletter/do/subscribe.php" onsubmit="return newsletter_check(this)">
			<div class="col-md-4 col-sm-12">
				<div class="form-group">
					<input class="form-control" placeholder="Nome" id="nn" type="text" name="nn" value="<?php echo $current_user->display_name ?>">
				</div>
			</div>
			<div class="col-md-4 col-sm-12">
				<div class="form-group">
					<input class="form-control" placeholder="Email" id="ne" type="email" name="ne" required value="<?php echo $current_user->user_email ?>">
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
	  				<label><input type="checkbox" name="ny" required>&nbsp;<a target="_blank" href="http://www.omaggiweb.it/data-privacy/">Autorizzo il trattamento dei miei dati personali ai sensi del D.Lgs. 196/2003</a></label>
				</div>
			</div>
			<div class="col-md-12">
				<input class="btn btn-success" type="submit" value="Iscriviti"/>
			</div>
		</form>
	</div>

	<?php
}

// Subscribe Newsletter Vertical Layout
function subscribe_newsletter_vertical() { 
	$current_user = wp_get_current_user(); ?>
	
	<script type="text/javascript">
		if (typeof newsletter_check !== "function") {
			window.newsletter_check = function (f) {
		    	var re = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-]{1,})+\.)+([a-zA-Z0-9]{2,})+$/;
		    	if (!re.test(f.elements["ne"].value)) {
		        	alert("L'indirizzo email entrato non e corretto!");
		        	return false;
		    	}
		    	if (f.elements["ny"] && !f.elements["ny"].checked) {
		        	alert("Dovrete accettare le regole per il trattamento dei vostri dati!");
		        	return false;
		    	}
		    	return true;
			}
		}
	</script>

	<div class="row subscribe">
		<form method="post" action="http://www.omaggiweb.it/wp-content/plugins/newsletter/do/subscribe.php" onsubmit="return newsletter_check(this)">
			<div class="col-md-12 col-sm-12">
				<div class="form-group">
					<input class="form-control" placeholder="Nome" id="nn" type="text" name="nn" value="<?php echo $current_user->display_name ?>">
				</div>
			</div>
			<div class="col-md-12 col-sm-12">
				<div class="form-group">
					<input class="form-control" placeholder="Email" type="email" id="ne" name="ne" required value="<?php echo $current_user->user_email ?>">
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
	  				<label><input type="checkbox" name="ny" required>&nbsp;<a target="_blank" href="http://www.omaggiweb.it/data-privacy/">Autorizzo il trattamento dei miei dati personali ai sensi del D.Lgs. 196/2003</a></label>
				</div>
			</div>
			<div class="col-md-12">
				<input class="btn btn-success" type="submit" value="Iscriviti"/>
			</div>
		</form>
	</div>

	<?php
}


function single_deal_image() {
    $deal_image = get_field('deal_image');
    $seller_name = get_field('seller_name');
    $seller_website = get_field('seller_website');
    if(!empty($deal_image)) { ?>
        <a href="<?php echo the_field('affiliate_link'); ?>" title="<?php the_title(); ?>" target="_blank">
            <img class="img-responsive img <?php echo $seller_name; ?>" src="<?php the_field('deal_image'); ?>" alt="<?php the_title(); ?>" >
        </a>
    <?php } else { ?>
        <a href="<?php echo the_field('affiliate_link'); ?>" title="<?php the_title(); ?>" target="_blank">
            <img class="img-responsive img <?php echo $seller_name; ?>" src="<?php if($seller_name != "Mondadori") { echo esc_url( get_stylesheet_directory_uri() ) . '/inc/img/default-deal.jpg'; } else { echo $seller_website; } ?>" alt="<?php the_title(); ?>" >
        </a>
    <?php }
}

function deal_image() {
    $deal_image = get_field('deal_image');
    $seller_name = get_field('seller_name');
    $seller_website = get_field('seller_website');
    if(!empty($deal_image)) { ?>
        <a href="<?php echo the_permalink(); ?>" title="<?php the_title(); ?>">
            <img class="img-responsive img <?php echo $seller_name; ?>" src="<?php the_field('deal_image'); ?>" alt="<?php the_title(); ?>" >
        </a>
   	<?php } else { ?>
        <a href="<?php echo the_permalink(); ?>" title="<?php the_title(); ?>">
            <img class="img-responsive img <?php echo $seller_name; ?>" src="<?php if($seller_name != "Mondadori") { echo esc_url( get_stylesheet_directory_uri() ) . '/inc/img/default-deal.jpg'; } else { echo $seller_website; } ?>" alt="<?php the_title(); ?>" >
        </a>
    <?php }
}