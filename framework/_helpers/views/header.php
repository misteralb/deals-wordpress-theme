<?php

// START Top Header Login Form
function header_login_form() {
	if(!is_user_logged_in()){ ?>
	
	<div class="accountbtn">
		<a href="account/" title="">Suggerisci Offerta</a>
	</div>
	<div id="loginFbBtn" class="accountbtn">
		<?php echo do_shortcode('[facebookall_login]'); ?>
	</div>
	<div id="loginBtn" class="accountbtn">
		<a href="#popup" title="" class="popup-link">Accedi</a>
	</div>
	<div id="registerBtn" class="accountbtn">
		<a href="account/" title="">Registrati</a>
	</div>

	<?php } else {
		$current_user = wp_get_current_user();
		$homeurl = home_url(); ?>
		
		<div class="accountbtn">
			<a href="<?php echo $homeurl; ?>/account/">Ciao <?php echo $current_user->first_name ?></a>
		</div>
		
		<?php if(current_user_can('administrator')) { ?>
		<div class="accountbtn">
   			<a href="<?php echo $homeurl; ?>/wp-admin/" target="_blank">WP-Admin</a>	
   		</div>
   		<?php } ?>

		<div class="accountbtn">
			<a href="<?php echo $homeurl; ?>/submit-deal/" title="">Suggerisci Offerta</a>
		</div>
	   	<div class="accountbtn">
	       	<a href="<?php echo wp_logout_url($homeurl); ?>">Esci</a>
       	</div>

	<?php }
} // END Top Header Login Form

// START New City Choser
function new_city_choser() {
	$locations = getLocations();
	$city = $_COOKIE['locality'];
	if(!isset($city)) { $city = "Italia"; }
	?>
	
	<div id="change-city-container">
		<div class="city-name">
			<span class="change-city-label">Scegli la tua città</span><br/>
			<span class="name">
				<b>in <b class="city"><?php echo $city; ?></b></b> &nbsp;<i class="fa fa-caret-down"></i>
			</span>
		</div>
		<div class="city-changer hide-item">
			 <select id="city-choser" value="" onchange="">
     			<option value="" selected="selected" disabled="disabled">Entrate provincia ...</option>
    			<?php foreach($locations as $location) { ?>
      			<option value="<?php echo $location; ?>"><?php echo $location; ?></option>
    			<?php } ?>
    		</select>
    		<input type="submit" id="local-deals" class="" value="Cambia Città">
		</div>
	</div>

	<div class="other-deals-changer">
		<?php if($city !== "Italia") : ?>
	    <input type="submit" value="Offerte Nazionali" id="national-deals">
	    <?php endif; ?>
	    <?php
	    if(is_user_logged_in()) {
	        // Personalised Experience Variables
	        $userinfo = userDetails();
	        $user_city = $userinfo->location;
	        $url = home_url();
	        if(isset($user_city) AND $user_city != "") { ?>
	            <input type="submit" value="Nella Mia Città" id="your-city-deals">
	            <script type="text/javascript">
	                jQuery("#your-city-deals").click(function(e){
	                    e.preventDefault();
	                    jQuery.removeCookie('locality', { path: '/' });
	                    jQuery.cookie('locality', '<?php echo $user_city; ?>', { path:'/' });
	                    window.location.href = '<?php echo $url; ?>';
                      	location.reload();
	                });
	            </script>
	        	<?php
	    	}
	    }
 	    $city_change_url = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	    ?>  	
  	</div>

  	<script type="text/javascript">
		
		// show city choser
  		jQuery(".city-name").click(function(){ jQuery(".city-changer").toggle(); });

      	// local deals
      	jQuery("#local-deals").click(function(){
        	var locality = jQuery("input.ui-autocomplete-input").val();
        	if(locality != "") {
          		jQuery.removeCookie('locality', { path: '/' });
           		jQuery.cookie('locality', locality, { path: '/' });
           		var url = "http://<?php echo $city_change_url; ?>";
          		window.location.href = url;
              location.reload(); 
        	}
      	});

      	// national deals
      	jQuery("#national-deals").click(function(){
        	// if(jQuery('body').hasClass('home')) {
            	jQuery.removeCookie('locality', { path: '/' });
            	jQuery.cookie('locality', 'Italia', { path: '/' });
            	window.location.href = "http://<?php echo $city_change_url; ?>";
              location.reload(); 
        	// } else { return false; }
      	});

      	// change city text when user changes city
      	var locality = jQuery.cookie('locality');
      	jQuery("span.name i.city").html(locality);
 	   
    </script>

	<?php
} // END New City Choser