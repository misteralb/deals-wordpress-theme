<?php

// START Slider for Homepage
function deals_homepage_slider() {
	
	// Personalised Experience Variables
    $userinfo = userDetails();
    $user_city = $userinfo->location;
    $user_gender = $userinfo->gender;
    $user_birthday = $userinfo->birthday;
    $city = $_COOKIE['locality'];
    $city_value = sliderCityChoser($city, $user_city);
    if(!isset($city_value)) { $city_value = "Italia"; }

    $query = query_posts(
        array(
            'post_type'         => 'deals',
            'meta_key'          => 'deal_status',
            'meta_value'        => 'Featured on Homepage',
            'deals_city'		=> $city_value,
            'posts_per_page'	=> 5,
            'meta_query' => array(
                array(
                    'key' => 'gender',
                    'value' => array($user_gender, 'Entrambi'),
                    'compare' => 'IN'
                )
            )
        )
    );

    if($query) :
    	?>
        <div id="homepage-slider-fullwidth">
            <div class="slider-container">
                <ul class="bxslider">

	                <?php
	                while (have_posts()) : the_post();
	                    $deal_image = get_field('deal_image');
	                    $deal_link = get_permalink();
	                    $deal_end_date = get_field('deal_end_date');
	                ?>

                    <li>
                        <?php deal_image(); ?>
                        <div class="slide-details">
                            <h3><a href="<?php echo $deal_link; ?>" title=""><?php echo the_title(); ?></a></h3>
                            <div class="details">
                                <div class="price-info">
                                    <span class="expire-date">L'offerta scade in: <?php echo $deal_end_date; ?></span><br/>
                                </div>
                                <div class="buy-now">
                                    <span><a class="buynowlink" href="<?php echo $deal_link; ?>" title="">Visualizza Offerta</a></span>
                                </div>
                            </div>
                        </div>
                    </li>

                	<?php
                	endwhile;
                	?>

                </ul>
            </div>
        </div>

        <?php
    	wp_reset_query();
    endif;
} // END Slider for homepage

// START Category Slider
function deals_category_slider() {

	// Personalised Experience Variables
    $userinfo = userDetails();
    $user_city = $userinfo->location;
    $user_gender = $userinfo->gender;
    $user_birthday = $userinfo->birthday;
    if(is_tax('deals_category')) { $category_name = get_queried_object()->name; } else { $category_name = ""; }
    $city = $_COOKIE['locality'];
    if(!isset($city)) { $city = "Italia"; }
    $city_value = sliderCityChoser($city, $user_city);

    $query = query_posts(
        array(
            'post_type'         => 'deals',
            'deals_category'    => $category_name,
            'meta_key'          => 'deal_status',
            'meta_value'        => 'Featured on Category Page',
            'deals_city'		=> $city_value,
            'meta_query' => array(
                array(
                    'key' => 'gender',
                    'value' => array($user_gender, 'Entrambi'),
                    'compare' => 'IN'
                )
            )
        )
    );
    if($query) {
    	?>

        <div id="homepage-slider-fullwidth">
            <div class="slider-container">
                <ul class="bxslider">

	                <?php
	                while (have_posts()) : the_post();
	                    $deal_status = get_field('deal_status');
	                    $deal_image = get_field('deal_image');
	                    $deal_link = get_permalink();
	                    $deal_end_date = get_field('deal_end_date');
	                ?>

                    <li>
                        <?php deal_image(); ?>
                        <div class="slide-details">
                            <h3><a href="<?php echo $deal_link; ?>" title=""><?php echo the_title(); ?></a></h3>
                            <div class="details">
                                <div class="price-info">
                                    <span class="expire-date">Deals Expires in: <?php echo $deal_end_date; ?></span><br/>
                                </div>
                                <div class="buy-now">
                                    <span><a class="buynowlink" href="<?php echo $deal_link; ?>" title="">Visualizza Offerta</a></span>
                                </div>
                            </div>
                        </div>
                    </li>

	                <?php
	                endwhile;
	                ?>

                </ul>
            </div>
        </div>

        <?php
    	wp_reset_query();
    }
} // END Category Slider