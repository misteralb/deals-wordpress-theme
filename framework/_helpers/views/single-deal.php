<?php

// START Related Posts Constructor
function related_posts() {
	// city
	$userinfo = userDetails();
    $user_city = $userinfo->location;
    $city = $_COOKIE['locality'];
    $city_value = winnerCity($city, $user_city);
    // category
    $terms = get_the_terms($post_ID, 'deals_category');
	foreach($terms as $term) {
		// get current deal category and show other deals from same category
      	// $args = array('post_type' => 'deals', 'posts_per_page' => 4, 'deals_city' => $city_value, 'deals_category' => $term->name, 'orderby' => 'rand');
      	// show as related deals only deals from coupon category
      	$args = array('post_type' => 'deals', 'posts_per_page' => 4, 'deals_city' => $city_value, 'deals_category' => 'Coupon', 'orderby' => 'rand');
    	query_posts($args);
    } ?>

 	<div class="row related-posts-container">
    	<div class="col-md-12">
      		<h4 class="related-posts">
        		<?php echo "Altre offerte"; ?>
      		</h4>
    	</div>

      	<?php
      	while(have_posts()) : the_post();
      		$deal_image = get_field('deal_image');
          	$deal_link = get_permalink();
      	?>

 		<div class="col-md-12">
	      	<div class="related-post-deal">
	         	<div class="dealimg">
	                <?php deal_image(); ?>
	              	<h4><a href="<?php echo get_permalink(); ?>" title=""><?php echo the_title(); ?></a></h4>
	          	</div>
	        </div>
        </div>

    	<?php endwhile; ?>
    </div>

    <?php
    wp_reset_query();
} // END Related Posts Constructor