<?php

// Hide Errors
ini_set("display_errors", 0);

// Use shortcodes on widgets
add_filter('widget_text', 'do_shortcode');

// Auto Publish on Social Networks using Jetpack
add_action('init', 'auto_share_jetpack_seller');
function auto_share_jetpack_seller() { add_post_type_support('deals', 'publicize'); }

// show admin bar only for admins
if(!current_user_can('manage_options')) { add_filter('show_admin_bar', '__return_false'); }
// show admin bar only for admins and editors
if(!current_user_can('edit_posts')) { add_filter('show_admin_bar', '__return_false'); }


add_filter('wp_mail_from_name', 'my_mail_from_name');
function my_mail_from_name($name) {
    return "OmaggiWeb.it";
}


// START List all costum fields for a deal
function getPostMeta() {
    echo '<h3>All Post Meta</h3>';
    $getPostCustom = get_post_custom();
    foreach($getPostCustom as $name => $value) {
        echo "<strong>" . $name . "</strong>"."  => ";
        foreach($value as $nameAr=>$valueAr) {
                echo "<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                echo $nameAr."  =>  ";
                echo var_dump($valueAr);
        }
        echo "<br/><br/>";
    }
} // START List all costum fields for a deal

// START Filter search results to show only Deals
function searchfilter($query) {
    if ($query->is_search && !is_admin() ) { $query->set('post_type',array('deals')); }
	return $query;
}
add_filter('pre_get_posts','searchfilter');
// END Filter search results to show only Deals

// START Costum Deals Feeds Template
remove_all_actions('do_feed_rss2');
add_action('do_feed_rss2', 'deals_feed', 10, 1);

function deals_feed($for_comments) {
    $rss_template = get_template_directory() . '/framework/_helpers/feeds/deals_feed.php';
    if(get_query_var('post_type') == 'deals' and file_exists($rss_template)) load_template($rss_template);
    else do_feed_rss2($for_comments);
} // END Costum Deals Feeds Template

// START Android Mobile Test
function wp_is_android() {
    static $is_android;
 
    if(isset($is_android)) return $is_android;
 
    if(empty($_SERVER['HTTP_USER_AGENT'])) {
        $is_android = false;
    } elseif(strpos($_SERVER['HTTP_USER_AGENT'], 'Android') !== false) {
        $is_android = true;
    } else {
        $is_android = false;
    }
 
    return $is_android;
} // END Android Mobile Test