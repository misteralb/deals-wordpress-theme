<?php 

// Helper for the submit deal page
function my_pre_save_post($post_id) {
    // check if this is to be a new post
    if($post_id != 'new') { return $post_id; }

    $title      = $_POST['deals_title'];
    $category   = $_POST['deals_category'];
    $city       = $_POST['deals_city'];

    // Create a new post
    $post = array(
        'post_status'       => 'draft',
        'post_title'        => $title,
        'deals_category'    => $category,
        'deals_city'        => $city,
        'post_type'         => 'deals',
    );  

    // insert the post
    $post_id = wp_insert_post($post); 
    // update $_POST['return']
    $_POST['return'] = add_query_arg(array('post_id' => $post_id), $_POST['return']);    
    // return the new ID
    return $post_id;
}
add_filter('acf/pre_save_post' , 'my_pre_save_post');

// Remove WP-Admin Styles on Frontend
add_action('wp_print_styles', 'remove_wpadmin_styles', 100);
function remove_wpadmin_styles() {
    wp_deregister_style('wp-admin');
}