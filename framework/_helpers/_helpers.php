<?php

require 'general.php';
require 'functional.php';
require 'get-data.php';
require 'submit-deals.php';
require 'user-personal-experience.php';

// CREATE VIEWS
require dirname( __FILE__ ) . '/views/common.php';
require dirname( __FILE__ ) . '/views/header.php';
require dirname( __FILE__ ) . '/views/sliders.php';
require dirname( __FILE__ ) . '/views/single-deal.php';