<?php

// Personalised Experience Helper
function userDetails() {
    $user_ID        = get_current_user_id();
    $location       = get_user_meta($user_ID, 'location', true);
    $birthday       = get_user_meta($user_ID, 'birthday', true);
    $gender         = get_user_meta($user_ID, 'gender', true);
    $userinfo 		= array('id' => $user_ID, 'location' => $location, 'birthday' => $birthday, 'gender' => $gender);
    $user = new stdClass();
    foreach ($userinfo as $key => $value) {
        $user->$key = $value;
    }
    return $user;
}

function calculateUserAge($user_birthday) {
    $user_birthyear = explode("/", $user_birthday);
    $birthyear = $user_birthyear['2'];
    $today_year = date('Y');
    $diffYear = $today_year - $birthyear;
    return $diffYear;
}