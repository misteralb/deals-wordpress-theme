<?php

// Calculate deal discounts
function savings($original_price, $discounted_price) {
    if($original_price != 0) {
        $discount   = $original_price - $discounted_price;
        $save     = ($discount/$original_price)*100;
        $savings  = number_format($save, 0);
        return $savings;
    } else if($original_price == 0 AND $discounted_price == 0) {
        return '100';
    } else {
      return "";
    }
}

// Countdown Timer for expiring deals
function countdown($deal_expire_date) {
    $date       = str_replace('/', '-', $deal_expire_date);
    $endDate    = date('Y-m-d', strtotime($date . "+1 day")); ?>
    <script type="text/javascript">
        var countdown = new Countdown({
            selector: '#timer',
            msgAfter: "Offerta Scaduta",
            msgPattern: "{days} giorni, {hours} ore e {minutes} minuti",
            dateEnd: new Date('<?php echo $endDate; ?>'),
        });
    </script>
    <?php
}

// Check if deal has expired
function is_expired($expire) {
    $now            = date("Y-m-d H:i:s");
    $expireDate     = str_replace('/', '-', $expire);
    $expireDate     = date('Y-m-d', strtotime($expireDate));

    if($now > $expireDate) {
        $expired = "yes";
    } else {
        $expired = "no";
    }
    return $expired;
}

// Get first post image
function get_first_image() {
  	global $post, $posts;
  	$first_img = '';
  	ob_start();
  	ob_end_clean();
  	$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
  	$first_img = $matches[1][0];
  	if(empty($first_img)) { $first_img = ""; }
  	return $first_img;
}

// Chose which city has priority on showing deals
function winnerCity($city, $user_city = "") {
    // CASE 1: city is setted --- city wins
    if(isset($city)) { $city_value = $city; }
    // CASE 2: city & user_city have values --- user_city wins
    if(isset($city) AND isset($user_city) AND $user_city != "") { $city_value = $user_city; }
    // CASE 3: city & user_city have no values --- national deals are shown
    if(!isset($city) AND !isset($user_city)) { $city_value = "Italia"; }
    // Return who is the winner
    return $city_value;
}

function sliderCityChoser($city, $user_city = "") {
    // CASE 1: city is setted --- city wins
    if(isset($city)) { $city_value = $city; }
    // CASE 2: city & user_city have values --- user_city wins
    if(isset($city) AND isset($user_city) AND $user_city != "") { $city_value = $city; }
    // CASE 3: city not set & city_user has value ---> city_user wins
    if(!isset($city) AND isset($user_city) AND $user_city != "") { $city_value = $user_city; }   
    // CASE 4: city & user_city have no values --- national deals are shown
    if(!isset($city) AND !isset($user_city)) { $city_value = "Italia"; }
    // Return who is the winner
    return $city_value;
}

// Check Deals Category
function inCategory() {
    $allowed_cats = array('Android', 'iPad', 'iPhone', 'Coupon da acquistare', 'Coupon da stampare', 'Kindle', 'Kobo', 'PDF', 'Viaggi');
    if(has_term($allowed_cats, 'deals_category')) { $catFlag = 1; }
    else { $catFlag = 0; }
    return $catFlag;
}

// Get Coupon Code for showing on single deal
function getCouponCode() {
    $sellerinfo = get_field('seller_name');
    $parts = explode("|", $sellerinfo);
    $couponcode = $parts['1'];
    return $couponcode;
}

// Clean Dates
function cleanDates($date) {
    $substr = substr($date, 0, -8);
    $parts = explode('-', $substr);
    $year = $parts['0'];
    $month = $parts['1'];
    $day = $parts['2'];
    $subDay = substr($day, 0, -1);
    $dateCleaned = $subDay . "/" . $month . "/" . $year;
    return $dateCleaned;
}

// Chose which 404 Error page to show based on the actual URL
function get404Type() {
    $actual_url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $home_url = home_url() . "/";
    $find_type =  str_replace($home_url, "", $actual_url);
    $type = explode("/", $find_type);
    return $type['0'];
}

// Convert dates to d/m/Y format
function convertDates($date) {
    $cleanDate = strtotime($date);
    $convertedDate = date('d/m/Y', $cleanDate);
    return $convertedDate;
}

function getOrigPrice($discp, $discount) {
    $origprice = ($discp * 100) / (100 - $discount);
    return $origprice;
}