<?php

function smartbox_parser() {
	
	// CONSTANTS
	$author_id = "24";
	$deal_location_id = "52";
	$deal_category_id = array('11', '12');
	$deal_gender = "Entrambi";
	$deal_age_groups = array('All Age Ranges');
	$seller_name = "SmartBox";
	$endDate = "31/12/2015";

	// Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
	require_once(ABSPATH . 'wp-admin/includes/image.php');

	$results = smartbox_curl();
	$products = $results->productItems->productItem;

	foreach($products as $product) {
		$deal_title = $product->name;
		$deal_description = $product->description;
		$deal_image = $product->image->medium;
		$deal_original_price = $product->price;
		$deal_start_date = $product->modified;
		$affiliate_link = $product->trackingLinks->trackingLink['0']->ppc;

		$startDate = convertDates($deal_start_date);

		// Save Deal Data to DB
		$page_check = get_page_by_title($deal_title, OBJECT, 'deals');
		if(!$page_check->ID){
			$post_id = wp_insert_post(array('post_author' => $author_id, 'post_type' => 'deals', 'post_status' => 'draft', 'post_title' => $deal_title));

			add_post_meta($post_id, 'seller_name', $seller_name);
			add_post_meta($post_id, 'deal_description', $deal_description);
			add_post_meta($post_id, 'affiliate_link', $affiliate_link);
			add_post_meta($post_id, 'original_price', $deal_original_price);
			add_post_meta($post_id, 'deal_start_date', $startDate);
			add_post_meta($post_id, 'deal_end_date', $endDate);
			add_post_meta($post_id, 'gender', $deal_gender);
			add_post_meta($post_id, 'age_range', $deal_age_groups);

			// Deal Category & Deal Location
			wp_set_post_terms($post_id, (array)$deal_category_id, 'deals_category', true);
			wp_set_post_terms($post_id, (array)$deal_location_id, 'deals_city', true);

			$deal_image_data = attach_deal_images($post_id, $deal_image);
			add_post_meta($post_id, 'deal_image', $deal_image_data['1']);

			echo "Deal <b>\"" . $deal_title . "\"</b> has been created successfully!<br/>";
		} else{
		    echo "Deal <b>\"" . $deal_title . "\"</b> already exists!<br/>";
		}
	}
}

add_shortcode('zanox_smartbox', 'smartbox_parser');