<?php

function getData($url) {
	// Retrieve Data from TradeDoubler
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$content = curl_exec($ch);
	// Parse Retrieved Data
	$data = json_decode($content);
	return $data;
}


function bitly_url_shorten($long_url) {
  	$url = 'https://api-ssl.bitly.com/v3/shorten?access_token=daf7106bca2e84a82fd96bb02f55c9e2f366d9f8&longUrl='. urlencode($long_url) . '&format=txt';
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_TIMEOUT, 4);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    $output = curl_exec($ch);
  	return $output;
}


function mondadoriStore($i) {
    $url = "https://api.import.io/store/data/b798581a-95f2-4c30-9150-d9c277147387/_query?input/webpage/url=http%3A%2F%2Fwww.mondadoristore.it%2FeBook-italiani-gratuiti%2Fgr-3046/".$i."/%2F%3Fsort%3D7%26opnedBoxes%3Damtp%252Catpp%252Cagen%252Capzf%252Caaut%252Caedt&_user=d9cd56eb-4fc0-4346-97cb-258ef39d5e12&_apikey=yKwxQB%2FmCtRic03s9iSns03kVJ0KOQ%2BUmoDGAL8zodFyFhqBdcSM60dqHUvUCsTCHTEIYrsC9aY4Mz8mqicEhA%3D%3D";
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_TIMEOUT, 4);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    $output = curl_exec($ch);
    return json_decode($output, false);
}

function amazon_curl($i) {
    $url = 'https://api.import.io/store/data/1932c1ab-e91a-4b7c-98d0-07aa9bdf1e5c/_query?input/webpage/url=http%3A%2F%2Fwww.amazon.it%2Fgp%2Fbestsellers%2Fdigital-text%2Fref%3Dzg_bs_digital-text_pg_'.$i.'%2F280-5594827-5194947%3Fie%3DUTF8%26pg%3D'.$i.'%26tf%3D1&_user=d9cd56eb-4fc0-4346-97cb-258ef39d5e12&_apikey=yKwxQB%2FmCtRic03s9iSns03kVJ0KOQ%2BUmoDGAL8zodFyFhqBdcSM60dqHUvUCsTCHTEIYrsC9aY4Mz8mqicEhA%3D%3D';
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_TIMEOUT, 4);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    $output = curl_exec($ch);
    return json_decode($output, false);
}

function smartbox_curl() {
    $url = 'https://api.zanox.com/json/2011-03-01/products?connectid=D25788744D0A62386DD4&programs=15615&items=50';
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_TIMEOUT, 4);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    $output = curl_exec($ch);
    return json_decode($output, false);
}