<?php

// Attach Image on Deal
function attach_deal_images($post_id, $deal_image) {
	// Download image during parsing
	$image = file_get_contents($deal_image);
	// Wordpress Upload Dirs
	$upload_dir = wp_upload_dir();
	$upload_path = $upload_dir['path'];
	$upload_url = $upload_dir['url'];
	// Image Filename
	$image_name = basename($deal_image);
	// Download File from external server to our server
	$upload_file = $upload_path . "/" . $image_name;
	file_put_contents($upload_file, $image);
	$deal_image = $upload_url . "/" . $image_name;

	// $filename should be the path to a file in the upload directory.
	$filename = $upload_file;
	// The ID of the post this attachment is for.
	$parent_post_id = $post_id;
	// Check the type of file. We'll use this as the 'post_mime_type'.
	$filetype = wp_check_filetype(basename($filename), null);

	// Prepare an array of post data for the attachment.
	$attachment = array(
	    'guid'           => $deal_image, 
	    'post_mime_type' => $filetype['type'],
	    'post_title'     => preg_replace( '/\.[^.]+$/', '', basename($filename)),
	    'post_content'   => '',
	    'post_status'    => 'inherit'
	);

	// Insert the attachment.
	$attach_id = wp_insert_attachment($attachment, $filename, $parent_post_id);

	// Generate the metadata for the attachment, and update the database record.
	$attach_data = wp_generate_attachment_metadata($attach_id, $filename);
	wp_update_attachment_metadata($attach_id, $attach_data);

	$dealImage = array($attach_id, $deal_image);
	return $dealImage;
}