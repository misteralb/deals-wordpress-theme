<?php

function deleteExpiredDeals(){
    global $wpdb;

    $currenttime = date("Y-m-d");

    # Set your threshold of max posts and post_type name
    $post_type = 'deals';

    # Query post type
    $query = "
        SELECT ID FROM $wpdb->posts
        WHERE post_type = '$post_type'
        AND post_status = 'publish'
        ORDER BY post_modified DESC
    ";
    $results = $wpdb->get_results($query);

    # Check if there are any results
    if(count($results)){
        foreach($results as $post){

            $customfield = get_field('deal_end_date');
            $datequery = "SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id = '$post->ID'";
            $dealfields = $wpdb->get_results($datequery);
            foreach($dealfields as $datafield) {
            	if($datafield->meta_key == "deal_end_date") { $deal_end_date = $datafield->meta_value; }
            }

   			$expireDate     = str_replace('/', '-', $deal_end_date);
            $expiration     = strtotime($expireDate . "+1 day");
    		$expireDate     = date('Y-m-d', $expiration);

    		if($deal_end_date == 'Non disponibile!') {
            	$purge = wp_delete_post($post->ID, true);
            }

            if ($expireDate < $currenttime) {
            	$purge = wp_delete_post($post->ID, true);
            }
        }
    }
}