<?php

// Create a new CRON-JOB interval
function monthly_interval($schedules) {
	$schedules['monthly'] = array('interval' => 2592000, 'display' => __('Monthly', 'wpdeals'));
	return $schedules;
}
add_filter('cron_schedules', 'monthly_interval'); 


// Main Deals Scheduler
add_action('wp', 'deals_scheduler');
function deals_scheduler() {
	if(!wp_next_scheduled('autopost_deals_daily')) { wp_schedule_event(time(), 'daily', 'autopost_deals_daily'); }
	if(!wp_next_scheduled('autopost_ebook_deals')) { wp_schedule_event(time(), 'monthly', 'autopost_ebook_deals'); }
}

add_action('autopost_deals_daily', 'daily_tasks');
function daily_tasks() {
	// tradeDoublerVouchers();
	tradeDoublerProducts();
	tradeDoublerHRS();
	tradeDoublerCrocierissime();
	deleteExpiredDeals();
}

add_action('autopost_ebook_deals', 'ebook_schedule');
function ebook_schedule() {
	mondadoriCrawlerParser();
	amazon_parser();
	smartbox_parser();
}