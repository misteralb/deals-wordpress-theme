<?php

function tradeDoublerCrocierissime() {
	$url = 'http://api.tradedoubler.com/1.0/products.jsonp;pretty=true;limit=100;fid=18299?token=51E0BD9E5A9420CF9061A8B751F550A21DE29C3F';
	$response = getData($url);
	$products = $response->products;

	// Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
	require_once(ABSPATH . 'wp-admin/includes/image.php');

	foreach($products as $product) {
		$fields = $product->fields;

		// DEAL CONSTANTS
		$author_id = "24";
		$deal_category_id = "182";
		$deal_location_id = "52";
		$deal_gender = "Entrambi";
		$deal_age_groups = array('18-24', '25-34', '35-44', '45-54', '55-64', '65+');
		$startDate = date('d/m/Y', time());

		$product_name = $product->name;
		$description = $product->description;
		$product_image = $product->productImage->url;

		$productURL = $product->offers['0']->productUrl;
		$short_afflink = bitly_url_shorten($productURL);
		$clean_link = substr($short_afflink, 0, -1);

		// get costum field variables
		foreach($fields as $field) {
			// deal end date
			if($field->name == "DataDiPartenza") {
				$endDate = $field->value;
				$endDateClean = convertDates($endDate);
			}
		}

		$price = $product->offers['0']->priceHistory;
		$current_price = $price['0']->price->value;

		// Save Deal Data to DB
		$page_check = get_page_by_title($product_name, OBJECT, 'deals');
		if(!$page_check->ID){
			$post_id = wp_insert_post(array('post_author' => $author_id, 'post_type' => 'deals', 'post_status' => 'draft', 'post_title' => $product_name));

			add_post_meta($post_id, 'deal_description', $description);
			add_post_meta($post_id, 'affiliate_link', $clean_link);
			add_post_meta($post_id, 'discounted_price', $current_price);
			add_post_meta($post_id, 'deal_start_date', $startDate);
			add_post_meta($post_id, 'deal_end_date', $endDateClean);
			add_post_meta($post_id, 'gender', $deal_gender);
			add_post_meta($post_id, 'age_range', $deal_age_groups);

			// Deal Category & Deal Location
			wp_set_post_terms($post_id, (array)$deal_category_id, 'deals_category', true);
			wp_set_post_terms($post_id, (array)$deal_location_id, 'deals_city', true);

			$deal_image_data = attach_deal_images($post_id, $product_image);
			add_post_meta($post_id, 'deal_image', $deal_image_data['1']);

			echo "Deal <b>\"" . $product_name . "\"</b> has been created successfully!<br/>";
		} else{
		    echo "Deal <b>\"" . $product_name . "\"</b> already exists!<br/>";
		}

	}

}

add_shortcode("tradedoubler_crocierissime", "tradeDoublerCrocierissime");