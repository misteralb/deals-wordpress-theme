<?php

// PARSE DATA
function tradeDoublerVouchers() {

	$url = "http://api.tradedoubler.com/1.0/vouchers.json?token=34E4747F173ABA99EB189C208188FC3BC2927B1D";
	$vouchers = getData($url);

	foreach($vouchers as $voucher) {
		// Parsing Deal Info
		$id 				= $voucher->id;
		$code 				= $voucher->code;
		$startDate 			= $voucher->startDate;
		$endDate 			= $voucher->endDate;
		$title 				= $voucher->title;
		$description 		= $voucher->description;
		$affiliate_link 	= $voucher->defaultTrackUri;
		$programName 		= $voucher->programName;
		$seller_website		= $voucher->landingUrl;
		// Process Deal Info
		$startDateCut 		= substr($startDate, 0, -3);
		$endDateCut 		= substr($endDate, 0, -3);
		$startDateClean 	= date('d/m/Y', $startDateCut);
		$endDateClean 		= date('d/m/Y', $endDateCut);

		// DEAL CONSTANTS
		$author_id = "24";
		$deal_location_id = "52";
		$deal_category_id = "8";
		$deal_gender = "Entrambi";
		$deal_age_groups = array('18-24', '25-34', '35-44', '45-54', '55-64', '65+');
		$seller_name = $programName . " | " . $code;

		// Save Deal Data to DB
		$page_check = get_page_by_title($title, OBJECT, 'deals');
		if(!$page_check->ID){
			$post_id = wp_insert_post(array('post_author' => $author_id, 'post_type' => 'deals', 'post_status' => 'draft', 'post_title' => $title));

			add_post_meta($post_id, 'seller_name', $seller_name);
			add_post_meta($post_id, 'seller_website', $seller_website);
			add_post_meta($post_id, 'deal_description', $description);
			add_post_meta($post_id, 'affiliate_link', $affiliate_link);
			add_post_meta($post_id, 'deal_start_date', $startDateClean);
			add_post_meta($post_id, 'deal_end_date', $endDateClean);
			add_post_meta($post_id, 'gender', $deal_gender);
			add_post_meta($post_id, 'age_range', $deal_age_groups);

			// Deal Category & Deal Location
			wp_set_post_terms($post_id, (array)$deal_category_id, 'deals_category', true);
			wp_set_post_terms($post_id, (array)$deal_location_id, 'deals_city', true);

			echo "Deal <b>\"" . $title . "\"</b> has been created successfully!<br/>";
		}else{
		    echo "Deal <b>\"" . $title . "\"</b> already exists!<br/>";
		}
		
	}
}
add_shortcode('tradedoubler_vouchers','tradeDoublerVouchers');