<?php

function tradeDoublerProducts() {
	$url = 'http://api.tradedoubler.com/1.0/products.jsonp;pretty=true;limit=100;fid=17282?token=51E0BD9E5A9420CF9061A8B751F550A21DE29C3F';
	$response = getData($url);
	$products = $response->products;

	// Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
	require_once(ABSPATH . 'wp-admin/includes/image.php');

	foreach($products as $product) {
		$offers = $product->offers;
		$fields = $product->fields;

		// DEAL CONSTANTS
		$author_id = "24";
		$deal_category_id = "12";
		$deal_gender = "Entrambi";
		$deal_age_groups = array('18-24', '25-34', '35-44', '45-54', '55-64', '65+');
		
		// DEAL VARIABLES
		$deal_title = $product->name;
		$deal_image = $product->productImage->url;
		$description = $product->description;

		$productUrl = $offers['0']->productUrl;
		$programName = $offers['0']->programName;
		$program_id = $offers['0']->id;

		// get costum field variables
		foreach($fields as $field) {
			// deal location
			if($field->name == "nameCity1") {
				$location_name = $field->value;
				$locations = getFullLocations();
				foreach($locations as $location) { if($location_name == $location->name) { $location_id = $location->term_id; } }
			}
			// deal start date
			if($field->name == "initDate") {
				$startDate = $field->value;
				$startDateClean = cleanDates($startDate);
			}
			// deal end date
			if($field->name == "endDate") {
				$endDate = $field->value;
				$endDateClean = cleanDates($endDate);
			}
			// deal original price
			if($field->name == "pvp") { $orig_price = $field->value; }
			// deal discounted price
			if($field->name == "finalpvp") { $disc_price = $field->value; }
			// deal costum url
			if($field->name == "customUrl1") { $customUrl1 = $field->value; }
		}

		$affiliate_link = 'http://pdt.tradedoubler.com/click?a(2354473)p(239129)product(' . $program_id . ')ttid(3)url(' . $customUrl1 . ')';		
		$short_afflink = bitly_url_shorten($affiliate_link);
		$clean_link = substr($short_afflink, 0, -1);
		
		// Save Deal Data to DB
		$page_check = get_page_by_title($deal_title, OBJECT, 'deals');
		if(!$page_check->ID){
			$post_id = wp_insert_post(array('post_author' => $author_id, 'post_type' => 'deals', 'post_status' => 'draft', 'post_title' => $deal_title));

			add_post_meta($post_id, 'seller_name', $programName);
			add_post_meta($post_id, 'deal_description', $description);
			add_post_meta($post_id, 'affiliate_link', $clean_link);
			add_post_meta($post_id, 'original_price', $orig_price);
			add_post_meta($post_id, 'discounted_price', $disc_price);
			add_post_meta($post_id, 'deal_start_date', $startDateClean);
			add_post_meta($post_id, 'deal_end_date', $endDateClean);
			add_post_meta($post_id, 'gender', $deal_gender);
			add_post_meta($post_id, 'age_range', $deal_age_groups);

			// Deal Category & Deal Location
			wp_set_post_terms($post_id, (array)$deal_category_id, 'deals_category', true);
			wp_set_post_terms($post_id, (array)$location_id, 'deals_city', true);

			$deal_image_data = attach_deal_images($post_id, $deal_image);
			add_post_meta($post_id, 'deal_image', $deal_image_data['1']);

			echo "Deal <b>\"" . $deal_title . "\"</b> has been created successfully!<br/>";
		} else{
		    echo "Deal <b>\"" . $deal_title . "\"</b> already exists!<br/>";
		}

	}

}

add_shortcode("tradedoubler_products", "tradeDoublerProducts");