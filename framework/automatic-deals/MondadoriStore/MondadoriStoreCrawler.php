<?php

function mondadoriCrawlerParser() {
	// CONSTANTS
	$author_id = "24";
	$deal_location_id = "52";
	$deal_category_id = "25";
	$deal_gender = "Entrambi";
	$deal_age_groups = array('All Age Ranges');
	$seller_name = "Mondadori";

	for($i=0; $i<10; $i++) {
		$result = mondadoriStore($i);
		$results = $result->results;

		foreach($results as $result) {

			$bookvote = "image_1/_alt";
			$book_vote = $result->$bookvote;
			
			if($book_vote != "voto 0 su 5") {
				$url = "http://www.mondadoristore.it";
				$book_image = $result->first_img_image;
				$book_link = $result->image_link_1;
				$titlelink = "title_link/_text";
				$book_title = $result->$titlelink;
				$author_name = "secondary_data_link/_text";
				$book_author = $result->$author_name;
				$startDate = date('d/m/Y');
				$endDate = date('d/m/Y', strtotime("+30 days"));

				$data = file_get_contents($book_link);
				preg_match_all("/<p class=\"text\" itemprop=\"description\">([^`]*?)<\/p>/", $data, $description);
				$description = $description['0']['0'];

				$deal_title = $book_title . " - " . $book_author;
				$affiliate_link = 'http://clkuk.tradedoubler.com/click?p(10388)a(2354473)g(19782026)url(' . $book_link . ')';

				// Save Deal Data to DB
				$page_check = get_page_by_title($deal_title, OBJECT, 'deals');
				if(!$page_check->ID){
					$post_id = wp_insert_post(array('post_author' => $author_id, 'post_type' => 'deals', 'post_status' => 'publish', 'post_title' => $deal_title));

					add_post_meta($post_id, 'seller_name', $seller_name);
					add_post_meta($post_id, 'affiliate_link', $affiliate_link);
					add_post_meta($post_id, 'deal_description', $description);
					add_post_meta($post_id, 'deal_start_date', $startDate);
					add_post_meta($post_id, 'deal_end_date', $endDate);
					add_post_meta($post_id, 'gender', $deal_gender);
					add_post_meta($post_id, 'age_range', $deal_age_groups);
					add_post_meta($post_id, 'seller_website', $book_image);

					// Deal Category & Deal Location
					wp_set_post_terms($post_id, (array)$deal_category_id, 'deals_category', true);
					wp_set_post_terms($post_id, (array)$deal_location_id, 'deals_city', true);

					echo "Deal <b>\"" . $deal_title . "\"</b> has been created successfully!<br/>";
				}else{
				    echo "Deal <b>\"" . $deal_title . "\"</b> already exists!<br/>";
				}
			}
		}
	}
}

add_shortcode("mondadori_store", "mondadoriCrawlerParser");