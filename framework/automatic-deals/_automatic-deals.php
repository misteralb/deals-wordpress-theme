<?php

// Task Scheduler
require dirname( __FILE__ ) . '/_helpers/_helpers.php';

// TradeDoubler
require dirname( __FILE__ ) . '/TradeDoubler/_TradeDoubler.php';

// MondadoriStore
require dirname( __FILE__ ) . '/MondadoriStore/_MondadoriStore.php';

// Amazon
require dirname( __FILE__ ) . '/Amazon/_Amazon.php';

// Zanox
require dirname( __FILE__ ) . '/Zanox/_Zanox.php';