<?php

function amazon_parser() {

	require_once 'lib/AmazonECS.class.php';
	$client = new AmazonECS('AKIAJB34JVOHOPAVUSXQ', 'txlXvLuCbdQQ6fz8tmrcITtnR7Ix3HiHftT2Nkdz', 'it', 'omag09-21');
	
	// CONSTANTS
	$author_id = "24";
	$deal_location_id = "52";
	$deal_category_id = "24";
	$deal_gender = "Entrambi";
	$deal_age_groups = array('All Age Ranges');
	$seller_name = "Mondadori";

	for($i=1; $i<6; $i++) {
		$results = amazon_curl($i);
		$products = $results->results;

		foreach($products as $product) {
			$product_price = $product->price_text;
			if($product_price == "Gratuito") {

				$book_link = $product->title_link;
				$parts = explode("/", $book_link);
				$asin = $parts['5'];

				$request = $client->responseGroup('Medium')->lookup($asin);
				$book_details = $request->Items->Item;

				$book_asin = $book_details->ASIN;
				$book_url = $book_details->DetailPageURL;
				$book_image = $book_details->LargeImage->URL;
				
				$book_attribs = $book_details->ItemAttributes;
				$book_author = $book_attribs->Author;
				$book_format = $book_attribs->Format;
				$book_nofpages = $book_attribs->NumberOfPages;
				$book_title = $book_attribs->Title . " - ebook Kindle";

				$book_review = $book_details->EditorialReviews->EditorialReview->Content;

				$startDate = date('d/m/Y');
				$endDate = date('d/m/Y', strtotime("+30 days"));
				
				$book_description = $book_review . "\n" . "Autore: " . $book_author . "\n" . "Formato: " . $book_format . "\n" . "Numero Pagine: " . $book_nofpages . "\n";

				// Save Deal Data to DB
				$page_check = get_page_by_title($book_title, OBJECT, 'deals');
				if(!$page_check->ID){
					$post_id = wp_insert_post(array('post_author' => $author_id, 'post_type' => 'deals', 'post_status' => 'publish', 'post_title' => $book_title));

					add_post_meta($post_id, 'seller_name', $seller_name);
					add_post_meta($post_id, 'seller_website', $book_image);
					add_post_meta($post_id, 'deal_description', $book_description);
					add_post_meta($post_id, 'affiliate_link', $book_url);
					add_post_meta($post_id, 'deal_start_date', $startDate);
					add_post_meta($post_id, 'deal_end_date', $endDate);
					add_post_meta($post_id, 'gender', $deal_gender);
					add_post_meta($post_id, 'age_range', $deal_age_groups);

					// Deal Category & Deal Location
					wp_set_post_terms($post_id, (array)$deal_category_id, 'deals_category', true);
					wp_set_post_terms($post_id, (array)$deal_location_id, 'deals_city', true);

					echo "Deal <b>\"" . $book_title . "\"</b> has been created successfully!<br/>";
				} else{
				    echo "Deal <b>\"" . $book_title . "\"</b> already exists!<br/>";
				}
			}
		}
	}
}

add_shortcode('amazon_ebooks', 'amazon_parser');