<?php

// DECLARE USER COSTUM FIELDS
add_action('show_user_profile', 'extra_user_profile_fields');
add_action('edit_user_profile', 'extra_user_profile_fields');

function extra_user_profile_fields($user) { ?>
	<h3><?php _e("Extra profile information", "blank"); ?></h3>
 
	<table class="form-table">
		<tr>
			<th><label for="birthday"><?php _e('Your Birthday', 'wpdeals'); ?></label></th>
			<td>
				<input type="text" name="birthday" id="birthday" value="<?php echo esc_attr(get_the_author_meta('birthday', $user->ID)); ?>" class="regular-text" /><br />
				<span class="description"><?php _e('Please enter your birthday.', 'wpdeals'); ?></span>
			</td>
		</tr>
		<tr>
			<th><label for="gender"><?php _e('Gender', 'wpdeals'); ?></label></th>
			<td>
				<input type="text" name="gender" id="gender" value="<?php echo esc_attr(get_the_author_meta('gender', $user->ID)); ?>" class="regular-text" /><br />
				<span class="description"><?php _e('Please enter your gender.', 'wpdeals'); ?></span>
			</td>
		</tr>
		<tr>
			<th><label for="location"><?php _e('Location', 'wpdeals'); ?></label></th>
			<td>
				<input type="text" name="location" id="location" value="<?php echo esc_attr(get_the_author_meta('location', $user->ID)); ?>" class="regular-text" /><br />
				<span class="description"><?php _e('Please chose your location.', 'wpdeals'); ?></span>
			</td>
		</tr>
	</table>
<?php
}