<?php



// Call Registration Form

function pippin_registration_form() {

	if(!is_user_logged_in()) {

		$registration_enabled = get_option('users_can_register');

		if($registration_enabled) {

			$output = pippin_registration_form_fields();

		} else {

			$output = __('User registration is not enabled', 'wpdeals');

		}

		return $output;

	}

}

add_shortcode('register_form', 'pippin_registration_form');



// Call Login Form

function pippin_login_form() {

	if(!is_user_logged_in()) {

		$output = pippin_login_form_fields();

	} else {

		// could show some logged in user info here

	}

	return $output;

}

add_shortcode('login_form', 'pippin_login_form');





// Registration Form

function pippin_registration_form_fields() { ob_start(); ?>	

		<h3 class="pippin_header"><?php _e('Registrati su OmaggiWeb', 'wpdeals'); ?></h3>

		

		<?php pippin_show_error_messages(); ?>

 

		<form class="pippin_form" action="" method="POST">

			<div class="row">

				<!-- Username -->

				<div class="col-md-6 col-sm-12">

					<div class="form-group">

						<label for="pippin_user_Login"><?php _e('Nome utente', 'wpdeals'); ?></label>

						<input class="form-control" name="pippin_user_login" id="pippin_user_login" class="required" type="text"/>

					</div>

				</div>

				<!-- Email -->

				<div class="col-md-6 col-sm-12">

					<div class="form-group">

						<label for="pippin_user_email"><?php _e('Email', 'wpdeals'); ?></label>

						<input class="form-control" name="pippin_user_email" id="pippin_user_email" class="required" type="email"/>

					</div>

				</div>

			</div>

			<div class="row">

				<!-- First Name -->

				<div class="col-md-6 col-sm-12">

					<div class="form-group">

						<label for="pippin_user_first"><?php _e('Nome', 'wpdeals'); ?></label>

						<input class="form-control" name="pippin_user_first" id="pippin_user_first" type="text"/>

					</div>

				</div>

				<!-- Last Name -->

				<div class="col-md-6 col-sm-12">

					<div class="form-group">

						<label for="pippin_user_last"><?php _e('Cognome', 'wpdeals'); ?></label>

						<input class="form-control" name="pippin_user_last" id="pippin_user_last" type="text"/>

					</div>

				</div>

			</div>

			<div class="row">

				<!-- Password -->

				<div class="col-md-6 col-sm-12">

					<div class="form-group">

						<label for="password"><?php _e('Password', 'wpdeals'); ?></label>

						<input class="form-control" name="pippin_user_pass" id="password" class="required" type="password"/>

					</div>

				</div>

				<!-- Confirm Password -->

				<div class="col-md-6 col-sm-12">

					<div class="form-group">

						<label for="password_again"><?php _e('Conferma Password', 'wpdeals'); ?></label>

						<input class="form-control" name="pippin_user_pass_confirm" id="password_again" class="required" type="password"/>

					</div>

				</div>

			</div>



			<div class="row verifiy-user">

				<div class="col-md-12">

					<div class="gglcptch">

						<style type="text/css" media="screen">

							#gglcptch_error { color: #F00; }

						</style>

						<script type="text/javascript">

							var ajaxurl = "http://localhost/omaggiweb/wp-admin/admin-ajax.php",

							gglcptch_error_msg = "Errore: Hai introdotto un valore CAPTCHA errato.";

						</script>

						<div class="g-recaptcha" data-sitekey="6LdeYQgTAAAAAK341kYTyDbuIKUhgKHazCfrDbTd" data-theme="light"></div>

						<script type="text/javascript" src="https://www.google.com/recaptcha/api.js"></script>

						<noscript>

							<div style="width: 302px; height: 352px;">

								<div style="width: 302px; height: 352px; position: relative;">

									<div style="width: 302px; height: 352px; position: absolute;">

										<iframe src="https://www.google.com/recaptcha/api/fallback?k=6LdeYQgTAAAAAK341kYTyDbuIKUhgKHazCfrDbTd" frameborder="0" scrolling="no" style="width: 302px; height:352px; border-style: none;"></iframe>

									</div>

									<div style="width: 250px; height: 80px; position: absolute; border-style: none; bottom: 21px; left: 25px; margin: 0px; padding: 0px; right: 25px;">

										<textarea id="g-recaptcha-response" name="g-recaptcha-response"	class="g-recaptcha-response" style="width: 250px; height: 80px; border: 1px solid #c1c1c1; margin: 0px; padding: 0px; resize: none;" value=""></textarea>

									</div>

								</div>

							</div>

						</noscript>

					</div>

				</div>

			</div>



			<?php // newsletter_register_form(); ?>

			

			<!-- Submit Button -->

			<div class="row">

				<div class="col-md-12">

					<div class="form-group">

						<input type="hidden" name="pippin_register_nonce" value="<?php echo wp_create_nonce('pippin-register-nonce'); ?>"/>

						<input class="btn btn-success" type="submit" value="<?php _e('Registrati', 'wpdeals'); ?>"/>

					</div>

				</div>

			</div>

		</form>



	<?php return ob_get_clean();

}





// Login Form

function pippin_login_form_fields() { ob_start(); ?>

		<h3 class="pippin_header"><?php _e('Login', 'wpdeals'); ?></h3>

 

		<?php pippin_show_error_messages(); ?>

 

		<form class="pippin_form" action="" method="post">

				<div class="row">

					<div class="costum-content col-md-12 col-sm-12">

						<div class="form-group">

							<label for="pippin_user_Login"><?php _e('Nome utente', 'wpdeals'); ?></label>

							<input class="form-control" name="pippin_user_login" id="pippin_user_login" class="required" type="text"/>

						</div>

						<div class="form-group">

							<label for="pippin_user_pass"><?php _e('Password', 'wpdeals'); ?></label>

							<input class="form-control" name="pippin_user_pass" id="pippin_user_pass" class="required" type="password"/>

						</div>

					</div>

				</div>



				<div class="row costum-submit">

					<div class="col-md-12">

						<div class="form-group">

							<input type="hidden" name="pippin_login_nonce" value="<?php echo wp_create_nonce('pippin-login-nonce'); ?>"/>

							<input class="btn btn-success login" id="pippin_login_submit" type="submit" value="Accedi"/>

						</div>

					</div>

				</div>

		</form>



	<?php return ob_get_clean();

}





// Login Functionality

function pippin_login_member() {

	if(isset($_POST['pippin_user_login']) && wp_verify_nonce($_POST['pippin_login_nonce'], 'pippin-login-nonce')) {

		$user = get_userdatabylogin($_POST['pippin_user_login']);

		if(!$user) { pippin_errors()->add('empty_username', __('Nome utente non valido', 'wpdeals')); }

		if(!isset($_POST['pippin_user_pass']) || $_POST['pippin_user_pass'] == '') { pippin_errors()->add('empty_password', __('Entra un password', 'wpdeals')); }

		if(!wp_check_password($_POST['pippin_user_pass'], $user->user_pass, $user->ID)) { pippin_errors()->add('empty_password', __('Password incorretto', 'wpdeals')); }

		

		$errors = pippin_errors()->get_error_messages();

		

		if(empty($errors)) {

			wp_setcookie($_POST['pippin_user_login'], $_POST['pippin_user_pass'], true);

			wp_set_current_user($user->ID, $_POST['pippin_user_login']);	

			do_action('wp_login', $_POST['pippin_user_login']);

			$location = $_SERVER['HTTP_REFERER'];

			wp_redirect($location); exit;

			exit;

		}

	}

}

add_action('init', 'pippin_login_member');





// Add new member

function pippin_add_new_member() {

  	if (isset($_POST["pippin_user_login"] ) && isset($_POST['g-recaptcha-response']) && wp_verify_nonce($_POST['pippin_register_nonce'], 'pippin-register-nonce')) {

		$user_login		= $_POST["pippin_user_login"];	

		$user_email		= $_POST["pippin_user_email"];

		$user_first 	= $_POST["pippin_user_first"];

		$user_last	 	= $_POST["pippin_user_last"];

		$user_pass		= $_POST["pippin_user_pass"];

		$pass_confirm 	= $_POST["pippin_user_pass_confirm"];



	    // Check Username

		if(username_exists($user_login)) { pippin_errors()->add('username_unavailable', __('Nome utente e gia esistente', 'wpdeals')); }

		if(!validate_username($user_login)) { pippin_errors()->add('username_invalid', __('Nome utente non valido', 'wpdeals')); }

		if($user_login == '') { pippin_errors()->add('username_empty', __('Entrate un nome utente', 'wpdeals')); }

		// Check Email

		if(!is_email($user_email)) { pippin_errors()->add('email_invalid', __('Email non valido', 'wpdeals')); }

		if(email_exists($user_email)) { pippin_errors()->add('email_used', __('Esiste un account con questo email', 'wpdeals')); }

		// Check Password

		if($user_pass == '') { pippin_errors()->add('password_empty', __('Entrate un password', 'wpdeals')); }

		if($user_pass != $pass_confirm) { pippin_errors()->add('password_mismatch', __('Passwords non sono compatibili', 'wpdeals')); }

 

		$errors = pippin_errors()->get_error_messages();

		if(empty($errors)) {

			$new_user_id = wp_insert_user(array(

					'user_login'		=> $user_login,

					'user_pass'	 		=> $user_pass,

					'user_email'		=> $user_email,

					'first_name'		=> $user_first,

					'last_name'			=> $user_last,

					'user_registered'	=> date('Y-m-d H:i:s'),

					'role'				=> 'subscriber'

				)

			);

			

			if($new_user_id) {

				wp_new_user_notification($new_user_id);

				wp_setcookie($user_login, $user_pass, true);

				wp_set_current_user($new_user_id, $user_login);

				do_action('wp_login', $user_login);

				$location = $_SERVER['HTTP_REFERER'];

				wp_redirect($location); exit;

			}

		}

	}

}

add_action('init', 'pippin_add_new_member');











// Show errors on registration page

function pippin_errors(){

    static $wp_error;

    return isset($wp_error) ? $wp_error : ($wp_error = new WP_Error(null, null, null));

}





// Errors

function pippin_show_error_messages() {

	if($codes = pippin_errors()->get_error_codes()) {

		echo '<div class="pippin_errors">';

		   foreach($codes as $code){

		        $message = pippin_errors()->get_error_message($code);

		        echo '<span class="error"><strong>' . __('Errore', 'wpdeals') . '</strong>: ' . $message . '</span><br/>';

		    }

		echo '</div>';

	}	

}