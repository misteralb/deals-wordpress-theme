<?php

// SAVE USER COSTUM FIELDS
add_action('personal_options_update', 'save_extra_user_profile_fields');
add_action('edit_user_profile_update', 'save_extra_user_profile_fields');
 
function save_extra_user_profile_fields($user_id) {
	if (!current_user_can('edit_user', $user_id)) { return false; }
	update_user_meta($user_id, 'birthday', $_POST['birthday']);
	update_user_meta($user_id, 'gender', $_POST['gender']);
	update_user_meta($user_id, 'location', $_POST['location']);
}