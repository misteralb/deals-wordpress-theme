<?php

function contest_page_content_full() { ?>
	<div class="contestpage">
		<div class="text-center">
			<h3>Contest mese Settembre 2015!</h3>
			<h5>Partecipate e invitate anche i vostri amici per avere piu grandi possibilita di vincere il contest ...</h5>
		</div>

	<?php

	if(is_user_logged_in()) {

		$linktoclick = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

		$friend1email = $_POST['friend1'];
		$friend2email = $_POST['friend2'];
		$friend3email = $_POST['friend3'];

		$emailsarray = array($friend1email, $friend2email, $friend3email);

		global $current_user;
  		get_currentuserinfo();

  		$user_fname 		= $current_user->user_firstname;
  		$user_lname 		= $current_user->user_lastname;
  		$user_email 		= $current_user->user_email;
  		$user_fullname		= $user_fname . " " . $user_lname;

		foreach($emailsarray as $email) {
			$to = $email;
			$subject = "Invito per concorso OmaggiWeb da " . $user_fullname;

			$message = "Voi siete stati invitati per participare al nostro Concorso Online OmaggiWeb da parte di " . $user_fullname . ".\n" .
					   "Se siete siccuri di voler partecipare cliccate qui ...\n" .
					   $linktoclick;


			$headers = 'From: '. $user_email . "\r\n" . 'Reply-To: ' . $user_email . "\r\n";

			if(isset($_POST['invite']) && isset($_POST['friend1'])) {
				$sent = wp_mail($to, $subject, strip_tags($message), $headers);
				if($sent) { $success = 1; }
			}
		}

		?>

		<div id="loggedin" class="text-center">
			<?php if($success != 1) { ?>
			
			<div id="phase2" class="row cpmaindiv">
				<div class="contestimage">
					<img src="<?php echo get_template_directory_uri() ?>/assets/img/contests/2015-09/02.jpg" id="contestimg" alt="" class="" />
				</div>

				<div class="contestdetails">
					<p class="maintext">
						Invitate i vostri amici a gioccare con noi per poter avere maggiori possibilita per vincere ...
					</p>

					<div id="inviteFriendsDivFB" class="invitefb">
						<?php echo do_shortcode('[fib appid="329080137214876"]'); ?>
					</div>

					<p>oppure, invitate tramitte email ...</p>

					<div id="inviteFriendsDiv" class="invitefriends">
						<form id="inviteFriendsFRM" action="" method="post">
							<input type="text" name="friend1" id="friend1" placeholder="E-mail amico #1" required/>
							<input type="text" name="friend2" id="friend2" placeholder="E-mail amico #2" />
							<input type="text" name="friend3" id="friend3" placeholder="E-mail amico #3" />
							<input type="submit" name="invite" value="Manda invito"/>
						</form>
					</div>

					<div class="show3image">
						<input type="submit" id="showlastimage" value="Scopri tutte le lettere">
					</div>
				</div>
			</div>

			<?php } else { ?>

			<div id="phase3" class="row cpmaindiv">
				<div class="contestimage">
					<img src="<?php echo get_template_directory_uri() ?>/assets/img/contests/2015-09/03.jpg" id="contestimg" alt="" class="" />
				</div>
				<div class="contestdetails">	
					<p class="maintext">
						Grazie per aver gioccato con noi ...
					</p>
					<div class="messagesent">
						<div class="alert alert-success" role="alert">Avete invitato i vostri amici con successo ...</div>
					</div>
				</div>
			</div>

			<?php } ?>

			<div class="fbcommentsandshare">
				<?php echo do_shortcode('[facebookall_comments]'); ?>
				<?php echo do_shortcode('[facebookall_share]'); ?>
			</div>

		</div>
	<?php 
	} else { ?>
		<div id="notloggedin" class="text-center cpmaindiv">
			<div id="phase1" class="row cpmaindiv">
				<div class="contestimage">
					<img src="<?php echo get_template_directory_uri() ?>/assets/img/contests/2015-09/01.jpg" id="contestimg" alt="" class="" />
				</div>
				<div class="contestdetails">
					<p class="maintext">
						Per partecipare al concorso dovrette essere registrati sul nostro sito ...
					</p>
					<div class="mainbtn">
						<a href="#popup" class="cpButtons">Accedi</a>
						<?php echo do_shortcode('[facebookall_login]'); ?>
						<a href="#registrati" class="cpButtons">Registrati</a>
					</div>
					<div id="registerDiv" class="hide-item cpregister">
						<?php echo do_shortcode('[register_form]'); ?>
					</div>
				</div>
			</div>
		</div>
		<?php
	}

	echo "</div>";
}

function contest_sept2015() { ?>
	<div class="contestpage">
		<div class="text-center">
			<h1>Contest Settembre 2015!</h1>
			<p>
				Individua nel paroliere le parole nascoste, che svelano la sorpresa in palio! Hai solo un tentativo! Trova per primo la soluzione per aggiudicarti l'omaggio!
			</p>
		</div>

	<?php

	if(is_user_logged_in()) { ?>

		<div id="loggedin" class="text-center">

			<?php if(!isset($_COOKIE['contestphase3'])) { ?>
			<div id="phase2" class="row cpmaindiv">
				<div class="contestimage">
					<img src="<?php echo get_template_directory_uri() ?>/assets/img/contests/2015-09/02.jpg" id="contestimg" />
				</div>
				<div class="contestdetails">
					<p class="maintext">
						Conferma i tuoi dati per visualizzare tutte le lettere del paroliere
					</p>
					<div id="subscribenewsletter" class="subscribediv">
						<?php
						$current_user = wp_get_current_user();
						echo do_shortcode('[mc4wp_form]');
						?>
						<script type="text/javascript">
							jQuery('input[name="FULLNAME"]').val("<?php echo $current_user->display_name; ?>");
							jQuery('input[name="EMAIL"]').val("<?php echo $current_user->user_email; ?>");
						</script>
					</div>
				</div>
			</div>
			<?php } else { ?>
			<div id="phase3" class="row cpmaindiv">
				<div class="contestimage">
					<img src="<?php echo get_template_directory_uri() ?>/assets/img/contests/2015-09/03.jpg" id="contestimg" />
				</div>
				<div class="contestdetails">
					<p class="maintext">
						Ora tutte le lettere sono visibili... prova ad individuare le parole nascoste scrivendole nei commenti in questa pagina.
					</p>
				</div>
			</div>
			<?php } ?>

		</div>

	<?php 
	} else { ?>
		<div id="notloggedin" class="text-center cpmaindiv">
			<div id="phase1" class="row cpmaindiv">
				<div class="contestimage">
					<img src="<?php echo get_template_directory_uri() ?>/assets/img/contests/2015-09/01.jpg" id="contestimg" alt="" class="" />
				</div>
				<div class="contestdetails">
					<p class="maintext">
						Accedi per partecipare al contest
					</p>
					<div class="mainbtn">
						<?php echo do_shortcode('[facebookall_login]'); ?>
					</div>
				</div>
			</div>
		</div>
	<?php } ?>

	</div>

	<div id="onlyloggedin" class="fbcommentsandshare">
		<?php echo do_shortcode('[facebookall_comments]'); ?>
		<?php echo do_shortcode('[facebookall_share]'); ?>
	</div>

	<?php
}

add_shortcode('contest_content_september_2015', 'contest_sept2015');