<?php

// Frontend Deals Submit Form
function fe_deals($content = null) {
	global $post;
	ob_start();
	
	do_action('fe_deals_notices');
	
	if(is_user_logged_in()) { ?>
		
		<form id="fe_deals_form" enctype="multipart/form-data" name="new_post" method="post" action="<?php the_permalink(); ?>">
			<div class="row">
				<div class="col-md-8">
					<div class="row deal-title">
						<div class="col-md-12">
							<div class="form-group">
								<label for="post-title">Titolo</label>
								<input class="form-control" type="text" name="post-title" id="post-title" />
							</div>
						</div>
					</div>
					<div class="row deal-info">
						<div class="col-md-12">
							<div class="form-group">
								<label for="acf-deal_description">Descrizione dell' Offerta</label>
								<?php
								$settings = array('media_buttons' => false, 'textarea_name' => 'acf-field-deal_description', 'textarea_rows' => 7);
								$editor_id = "acf-field-deal_description";
								wp_editor($content, $editor_id, $settings);
								?>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label for="acf-field-affiliate_link">Sito</label>
								<input type="text" class="form-control" name="acf-field-affiliate_link" id="acf-field-affiliate_link" />
							</div>
						</div>
						<div class="col-md-6 col-sm-12">
							<div class="form-group">
								<label for="acf-deal_start_date">Data d'inizio offerta</label>
								<input class="form-control" type="text" name="acf-field-deal_start_date" id="acf-field-deal_start_date" />
							</div>
						</div>
						<div class="col-md-6 col-sm-12">
							<div class="form-group">
								<label for="acf-deal_end_date">Data fine offerta</label>
								<input class="form-control" type="text" name="acf-field-deal_end_date" id="acf-field-deal_end_date" />
							</div>
						</div>
					</div> 
				</div>
				<div class="col-md-4">
					<div class="row categories">
						<div class="col-md-12">
							<legend>Scegli categoria/e:</legend>
							<?php $categories = getParentCategories();
							foreach ($categories as $category) { ?>
								<div class="checkbox">
  									<label><input type="checkbox" name="deals_category[]" value="<?php echo $category->term_id; ?>"><?php echo $category->name; ?></label>
								</div>
							<?php } ?>
						</div>
					</div>
					<div class="row locations">
						<div class="col-md-12">
							<legend>Indica le città in cui vale l'offerta:</legend>
							<select data-placeholder="Seleziona città ..." name="deals_city[]" id="deals_city" multiple class="chosen-select">
							<?php $locations = getFullLocations();
							foreach ($locations as $location) { ?>
		                		<option value="<?php echo $location->term_id; ?>"><?php echo $location->name; ?></option>
							<?php } ?>
							</select>
						</div>
					</div>
				</div>
			</div>

			<script type="text/javascript">
				var $j = jQuery.noConflict();
			   	$j('#acf-field-deal_start_date').datepicker({ format: "dd/mm/yyyy" });
			   	$j('#acf-field-deal_end_date').datepicker({ format: "dd/mm/yyyy" });
			   	$j("#deals_city").chosen();
			</script>

			<input id="submit" type="submit" class="btn btn-success" tabindex="3" value="<?php esc_attr_e('Segnalate Offerta'); ?>" />					
			<input type="hidden" name="action" value="post" />
			<input type="hidden" name="empty-description" id="empty-description" value="1"/>
			<?php wp_nonce_field('new-post'); ?>
		</form>

		<style>
			body.page .wp-editor-container { border: 1px solid #ddd; }
			body.page .wp-editor-tabs { display: none !important; }
			#deals_category { width: 100%; height: 300px; }
			#deals_city { width: 100%; }
			#deals_city_chzn { width: 100% !important; }
			#deals_city_chzn .chzn-drop { width: 100% !important; }
		</style>
		
		<?php
	} else {		
		echo "<h4>Per segnalare un'offerta <a href='http://www.omaggiweb.it/account/#register' title=''>Registrati</a> o fai <a href='http://www.omaggiweb.it/account/#login' title=''>Login</a></h4>";	
	}

	$output = ob_get_contents();
	ob_end_clean();
	if(is_page()) return $output;
}
add_shortcode('fe_deals', 'fe_deals');

// Errors
function fe_deals_errors(){ ?>
	<style>
		.fe_deals_errors { border: 1px solid #CC0000; border-radius: 3px; background-color: #FFEBE8; margin: 0 0 8px 0px; padding: 6px; }
	</style>
	<?php
	global $error_array;
	foreach($error_array as $error){ echo '<p class="fe_deals_errors">' . $error . '</p>'; }
}

// Notifications
function fe_deals_notices(){ ?>
	<style>
		.fe_deals_notices { border: 1px solid #E6DB55; border-radius: 3px; background-color: #FFFBCC; margin: 0 0 8px 0px; padding: 6px; }
	</style>
	<?php
	global $notice_array;
	foreach($notice_array as $notice){ echo '<p class="fe_deals_notices">' . $notice . '</p>'; }
}

// Frontend Deals (Business Logic)
function fe_add_deal(){
	if('POST' == $_SERVER['REQUEST_METHOD'] && !empty($_POST['action']) && $_POST['action'] == 'post'){
		if(!is_user_logged_in()) return;
		global $current_user;

		// General DEAL Info
		$user_id			= $current_user->ID;
		$post_title     	= $_POST['post-title'];
		// Deal Category & Deal City
		$deal_category		= $_POST['deals_category'];
		$deal_location		= $_POST['deals_city'];
		// Deal Info
		$deal_description	= $_POST['acf-field-deal_description']; 	// deal_description
		$affiliate_link		= $_POST['acf-field-affiliate_link']; 		// affiliate_link
		$deal_start_date	= $_POST['acf-field-deal_start_date']; 		// deal_start_date
		$deal_end_date		= $_POST['acf-field-deal_end_date']; 		// deal_end_date

		global $error_array;
		$error_array = array();
 	
 		// Create Errors Array
		if(empty($post_title)) $error_array[] 			= "Inserite un titolo per l'offerta!";
		if(empty($deal_description)) $error_array[]		= "Inserite una descrizione per l'offerta!";
		if(empty($affiliate_link)) $error_array[]		= "Inserite un link per l'offerta!";
 
		if(count($error_array) == 0){
			// Insert main Deal info
			$post_id = wp_insert_post(array('post_author' => $user_id, 'post_type' => 'deals', 'post_status' => 'draft', 'post_title' => $post_title));

			// Deal Info
			add_post_meta($post_id, 'deal_description', $deal_description);
			add_post_meta($post_id, 'affiliate_link', $affiliate_link);
			add_post_meta($post_id, 'deal_start_date', $deal_start_date);
			add_post_meta($post_id, 'deal_end_date', $deal_end_date);
			add_post_meta($post_id, 'frontend_deal', "1");

			// Deal Category & Deal Location
			wp_set_post_terms($post_id, (array)$deal_category, 'deals_category', true);
			wp_set_post_terms($post_id, (array)$deal_location, 'deals_city', true);
 
			global $notice_array;
			$notice_array = array();
			$notice_array[] = "Grazie per la tua segnalazione. Il nostro team pubblicherà la tua offerta entro 5 giorni lavorativi se la riterrà attinente ed interessante per i nostri utenti.";
			add_action('fe_deals_notices', 'fe_deals_notices');
		} else {
			add_action('fe_deals_notices', 'fe_deals_errors');
		}
	}
}
add_action('init','fe_add_deal');