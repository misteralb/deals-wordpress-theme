<?php

// START Featured Deals & Sorter
add_shortcode("featured_deals", "featured_deals");
function featured_deals($atts) {

	// Personalised Experience Variables
    $userinfo = userDetails();
    $user_city = $userinfo->location;
    $user_gender = $userinfo->gender;
    if(is_tax('deals_category')) { $category_name = get_queried_object()->name; } else { $category_name = ""; }
    $city = $_COOKIE['locality'];
    if(!isset($city)) { $city = "Italia"; }

    $atts = shortcode_atts(array('city' => '', 'category' => ''), $atts);
    $atts['city'] = winnerCity($city, $user_city);

    $args = array(
        'post_type' 		=> 'deals',
        'posts_per_page' 	=> 4,
        'meta_key' 			=> 'deal_priority',
        'meta_value' 		=> array('1', '2', '3', '4', '5'),
        'orderby'			=> 'rand',
        'deals_city'		=> $atts['city'],
        'deals_category'	=> $category_name,
        'meta_query' => array(
            array(
                'key' => 'gender',
                'value' => array($user_gender, 'Entrambi'),
                'compare' => 'IN'
            )
        )
    );
    $results = query_posts($args);
    if(count($results) < 1) { echo ""; }
    else {
    ?>

    <div class="row dealtitle">
        <div class="col-md-12">
            <h2 class="pull-left">Offerte in Evidenza</h2>
            <?php if(!is_tax('deals_category')) { deals_sorter(); } ?>    
        </div>
    </div>
    <div class="row deals">
	    <?php
	    while (have_posts()) : the_post();
	        $deal_image = get_field('deal_image');
	        $deal_link = get_permalink();
	        $deal_end_date = get_field('deal_end_date');
	    ?>
	    <div class="col-md-3 dealpanel featured">
	        <div class="dealimg">
	            <?php deal_image(); ?>
	        </div>
	        <div class="dealdetails">
	            <h4><a href="<?php echo $deal_link; ?>" title=""><?php echo the_title(); ?></a></h4>
	            <span class="expire-date"><em>Disponibile fino al:</em> <b><?php echo $deal_end_date; ?></b></span>
	            <span class="buynow"><a href="<?php echo $deal_link; ?>" title="">Visualizza Offerta</a></span>
	        </div>
	    </div>
    	<?php
    	endwhile;
    	wp_reset_query(); ?>
    </div>
    <?php }
} // END Featured Deals & Sorter

// START Deals Listing Constructor
add_shortcode("deals", "deals_listing");
function deals_listing($atts) {
    $atts = shortcode_atts(array('city' => 'Italia', 'category' => '', 'sortby' => '', 'posts' => ''), $atts);
    
    $userinfo = userDetails();
    $user_city = $userinfo->location;
    $user_gender = $userinfo->gender;

    $sort_order = $atts['sortby'];
    switch($sort_order) {
	    case "recent":
	        $args = array(
		        'post_type'         => 'deals',
		        'posts_per_page'    => $atts['posts'],
		        'deals_city'    	=> $atts['city'],
		        'deals_category'	=> $atts['category'],
		        'order'             => 'DESC',
                'meta_query' => array(
                    array(
                        'key' => 'gender',
                        'value' => array($user_gender, 'Entrambi'),
                        'compare' => 'IN'
                    )
                )
		    );
	        break;
	    case "popular":
	        $args = array(
		        'post_type'         => 'deals',
		        'posts_per_page'    => $atts['posts'],
		        'deals_city'    	=> $atts['city'],
		        'deals_category'	=> $atts['category'],
		        'orderby'           => 'meta_value_num',
                'meta_key'          => 'post_views_count',
		        'order'             => 'DESC',
                'meta_query' => array(
                    array(
                        'key' => 'gender',
                        'value' => array($user_gender, 'Entrambi'),
                        'compare' => 'IN'
                    )
                )
		    );
	        break;
	    case "nearest":
	        $args = array(
		        'post_type'         => 'deals',
		        'posts_per_page'    => $atts['posts'],
		        'deals_city'    	=> $atts['city'],
		        'deals_category'	=> $atts['category'],
		        'order'             => 'DESC',
                'meta_query' => array(
                    array(
                        'key' => 'gender',
                        'value' => array($user_gender, 'Entrambi'),
                        'compare' => 'IN'
                    )
                )
		    );
	        break;
	    default:
	        $args = array(
		        'post_type'         => 'deals',
		        'posts_per_page'    => $atts['posts'],
		        'deals_city'    	=> $atts['city'],
		        'deals_category'	=> $atts['category'],
		        'orderby'           => 'DESC',
                'meta_query' => array(
                    array(
                        'key' => 'gender',
                        'value' => array($user_gender, 'Entrambi'),
                        'compare' => 'IN'
                    )
                )
		    );
	}

    // Personalised Experience Variables
    $userinfo = userDetails();
    $user_city = $userinfo->location;
    $city = $_COOKIE['locality'];

    $category_data = get_term_by('slug', $atts['category'], 'deals_category');
    $category_slug = $category_data->slug;
    $caturl = home_url() . "/deals_category/" . $category_slug;

    $results = query_posts($args);
    if(count($results) < 1) { ?>

        <div class="row dealtitle">
            <div class="col-md-12">
                <?php if(is_tax('deals_category')) { ?>
                <h2 class="pull-left">
                    <?php
                    // city and user_city have values
                    if(isset($user_city) AND isset($city) AND $user_city != "" AND $city != "") {
                        if($city == "Italia") { ?>
                            <a href="<?php echo $caturl; ?>" title="">Offerte nazionali in <?php echo $atts['category']; ?></a>
                        <?php } else { ?>
                            <a href="<?php echo $caturl; ?>" title=""><?php echo $atts['category'] . " a " . $city; ?></a>
                        <?php }
                    }
                    // only city has value
                    else if(!isset($user_city) AND isset($city) AND $city != "") {
                        if($city == "Italia") { ?>
                            <a href="<?php echo $caturl; ?>" title="">Offerte nazionali in <?php echo $atts['category']; ?></a>;
                        <?php } else { ?>
                            <a href="<?php echo $caturl; ?>" title=""><?php echo $atts['category'] . " a " . $city; ?></a>
                        <?php }
                    }
                    // only user_city has value
                    else if(isset($user_city) AND !isset($city) AND $user_city != "") {
                        if($city == "Italia") { ?>
                            <a href="<?php echo $caturl; ?>" title="">Offerte nazionali in <?php echo $atts['category']; ?></a>;
                        <?php } else { ?>
                            <a href="<?php echo $caturl; ?>" title=""><?php echo $atts['category'] . " a " . $user_city; ?></a>
                        <?php }
                    }
                    // city and user_city have no value
                    else { ?>
                        <a href="<?php echo $caturl; ?>" title=""><?php echo $atts['category']; ?></a>
                    <?php }
                    ?>
                </h2>
                <?php } else if (is_tax('deals_city')) { ?>
                <h2 class="pull-left"><a href="" title=""><?php echo $atts['city']; ?></a></h2>
                <?php } ?>
            </div>
        </div>

        <div class="row deals">
            <div class="col-md-12">
                <p class="margin-top-10 nodeals"><big>Nessuna offerta disponibile, guarda tra le offerte <span id="show-nationals">Nazionali</span></big></p>
            </div>
        </div>

    <?php } else { ?>

	    <div class="row dealtitle">
	        <div class="col-md-12">
				<?php if(is_tax('deals_category')) { ?>
	            <h2 class="pull-left">
                    <?php
                    // city and user_city have values
                    if(isset($user_city) AND isset($city) AND $user_city != "" AND $city != "") {
                        if($city == "Italia") { ?>
                            <a href="<?php echo $caturl; ?>" title="">Offerte nazionali in <?php echo $atts['category']; ?></a>
                        <?php } else { ?>
                            <a href="<?php echo $caturl; ?>" title=""><?php echo $atts['category'] . " a " . $city; ?></a>
                        <?php }
                    }
                    // only city has value
                    else if(!isset($user_city) AND isset($city) AND $city != "") {
                        if($city == "Italia") { ?>
                            <a href="<?php echo $caturl; ?>" title="">Offerte nazionali in <?php echo $atts['category']; ?></a>;
                        <?php } else { ?>
                            <a href="<?php echo $caturl; ?>" title=""><?php echo $atts['category'] . " a " . $city; ?></a>
                        <?php }
                    }
                    // only user_city has value
                    else if(isset($user_city) AND !isset($city) AND $user_city != "") {
                        if($city == "Italia") { ?>
                            <a href="<?php echo $caturl; ?>" title="">Offerte nazionali in <?php echo $atts['category']; ?></a>;
                        <?php } else { ?>
                            <a href="<?php echo $caturl; ?>" title=""><?php echo $atts['category'] . " a " . $user_city; ?></a>
                        <?php }
                    }
                    // city and user_city have no value
                    else { ?>
                        <a href="<?php echo $caturl; ?>" title=""><?php echo $atts['category']; ?></a>
                    <?php }
                    ?>
                </h2>
	            <?php } else if (is_tax('deals_city')) { ?>
	            <h2 class="pull-left"><a href="" title=""><?php echo $atts['city']; ?></a></h2>
	            <?php } ?>
	        </div>
	    </div>

	    <div class="row deals">
		    <?php
		    while(have_posts()) : the_post();
		        $deal_image = get_field('deal_image');
		        $deal_link = get_permalink();
		        $deal_end_date = get_field('deal_end_date');
		    ?>
		    <div class="col-md-3 col-sm-6 dealpanel">
		        <div class="dealimg">
		            <?php deal_image(); ?>
		        </div>
		        <div class="dealdetails">
		            <h4><a href="<?php echo $deal_link; ?>" title=""><?php echo the_title(); ?></a></h4>
		            <span class="expire-date"><em>Disponibile fino al:</em> <b><?php echo $deal_end_date; ?></b></span>
		            <span class="buynow"><a href="<?php echo $deal_link; ?>" title="">Visualizza Offerta</a></span>
		        </div>
		    </div>
		    <?php
		    endwhile;
		    wp_reset_query(); ?>
	    </div>
	    
    <?php }
} // END Deals Listing Constructor