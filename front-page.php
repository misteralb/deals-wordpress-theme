<?php
/*
 Template: 	Front Page
 Modified: 	26/01/2015
 Author:	Arber Braja
*/
?>

<?php
if (get_option('show_on_front') == 'posts') { get_template_part('index'); }
elseif ('page' == get_option('show_on_front')) { get_header(); ?>

	<div id="primary" class="content-area col-sm-12 col-md-12">
		<main id="main" class="site-main" role="main">

			<?php
			
			echo do_shortcode('[featured_deals]');

			$sortOrder = $_GET['sortby'];
			$city = $_COOKIE['locality'];
			
			if(!isset($city)) {
				echo do_shortcode('[deals category="Omaggi" city="Italia" posts="4" sortby="' . $sortOrder . '"]');
				echo do_shortcode('[deals category="Sconti" city="Italia" posts="4" sortby="' . $sortOrder . '"]');
				echo do_shortcode('[deals category="Coupon" city="Italia" posts="4" sortby="' . $sortOrder . '"]');
			} else {
				echo do_shortcode('[deals category="Omaggi" city="' . $city . '" posts="4" sortby="' . $sortOrder . '"]');
				echo do_shortcode('[deals category="Sconti" city="' . $city . '" posts="4" sortby="' . $sortOrder . '"]');
				echo do_shortcode('[deals category="Coupon" city="' . $city . '" posts="4" sortby="' . $sortOrder . '"]');
			}

			?>

		</main>
	</div>
	
<?php get_footer(); } ?>