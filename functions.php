<?php

// THEME SETUP
if(!function_exists('unite_setup')) {
	function unite_setup() {
		load_theme_textdomain('unite', get_template_directory() . '/languages');
		add_theme_support('automatic-feed-links');
		add_theme_support('post-thumbnails');
		add_image_size('unite-featured', 730, 410, true);
		add_image_size('tab-small', 60, 60 , true);
		register_nav_menus(array('primary' => __('Primary Menu', 'wpdeals'), 'footer-links' => __('Footer Menu', 'wpdeals')));
	}
}
add_action('after_setup_theme', 'unite_setup');


// ADD SCRIPTS TO HEAD
function unite_scripts() {
  	// Queue Styles
  	wp_enqueue_style('unite-bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css');
	wp_enqueue_style('unite-icons', get_template_directory_uri() . '/assets/css/font-awesome.min.css');
	wp_enqueue_style('datepicker', get_template_directory_uri() . '/assets/css/datepicker.css');
	wp_enqueue_style('chosen', get_template_directory_uri() . '/assets/css/chosen.css');
	wp_enqueue_style('unite-style', get_stylesheet_uri());
	wp_enqueue_style('costum-style', get_template_directory_uri() . '/framework/costum.css');
	// Queue Scripts
	wp_enqueue_script('unite-bootstrapjs', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array('jquery'));
	wp_enqueue_script('unite-functions', get_template_directory_uri() . '/assets/js/main.min.js', array('jquery'));
	wp_enqueue_script('bxslider-js', get_template_directory_uri() . '/assets/js/jquery.bxslider.min.js', array('jquery'));
	wp_enqueue_script('chosen-js', get_template_directory_uri() . '/assets/js/jquery.chosen.js', array('jquery'));
	wp_enqueue_script('jqcookie', get_template_directory_uri() . '/assets/js/jquery.cookie.js', array('jquery'));
	wp_enqueue_script('countdown', get_template_directory_uri() . '/assets/js/countdown.min.js', array('jquery'));
	wp_enqueue_script('jquery-ui', get_template_directory_uri() . '/assets/js/jquery-ui.min.js', array('jquery'));
	wp_enqueue_script('jquery-autocomplete', get_template_directory_uri() . '/assets/js/jquery.select-to-autocomplete.js', array('jquery'));
	wp_enqueue_script('jquery-datepicker', get_template_directory_uri() . '/assets/js/bootstrap-datepicker.js', array('jquery'));
}
add_action('wp_enqueue_scripts', 'unite_scripts');


// TEMPLATE CONTENT WIDTH
function unite_content_width() {
  	if(is_page_template('tmpl_page-fullwidth.php') || is_page_template('front-page.php')) {
    	global $content_width;
    	$content_width = 1110;
  	}
}
add_action('template_redirect', 'unite_content_width');




// INCLUDE ADMIN OPTIONS FRAMEWORK
define('OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/inc/admin/');
require_once dirname( __FILE__ ) . '/inc/admin/options-framework.php';

// EXTERNAL INCLUDES
require get_template_directory() . '/inc/template-tags.php';
require get_template_directory() . '/inc/extras.php';
require get_template_directory() . '/inc/navwalker.php';
require get_template_directory() . '/inc/widgets.php';


/****************************/
/****** MODIFICATIONS *******/
/****************************/

require get_template_directory() . '/framework/_deals.php';