<?php
/*
 Template: 	Sidebar
 Modified: 	26/01/2015
 Author:	Arber Braja
*/
?>

<?php
$post_class = get_post_class();
if($post_class[1] == "deals") {
?>

	<div id="secondary" class="widget-area col-sm-12 col-md-3" role="complementary">

		<?php related_posts(); ?>
			
	</div>

<?php } else { ?>

	<div id="secondary" class="widget-area col-sm-12 col-md-4" role="complementary">
		<?php do_action('before_sidebar'); ?>
		<?php if(!dynamic_sidebar('sidebar-1')) : ?>

		<aside id="search" class="widget widget_search">
			<?php get_search_form(); ?>
		</aside>

		<aside id="archives" class="widget">
			<h1 class="widget-title"><?php _e('Archivio', 'wpdeals'); ?></h1>
			<ul>
				<?php wp_get_archives( array('type' => 'monthly')); ?>
			</ul>
		</aside>

		<aside id="meta" class="widget">
			<h1 class="widget-title"><?php _e( 'Login', 'wpdeals' ); ?></h1>
			<ul>
				<?php wp_register(); ?>
				<li><?php wp_loginout(); ?></li>
				<?php wp_meta(); ?>
			</ul>
		</aside>

		<?php endif; ?>
	</div>

<?php } ?>
