<?php
/*
 Template: 	Single Deal
 Modified: 	26/01/2015
 Author:	Arber Braja
*/
 ?>

<!-- TradeDoubler Tracking -->
<script type="text/javascript">
	var uri = 'http://impit.tradedoubler.com/imp?type(inv)g(22509100)a(2354473)' + new String (Math.random()).substring (2, 11);
	document.write('<img class="tdtracking" src="'+uri +'">');
</script>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header page-header">
		<h1 class="entry-title "><?php the_title(); ?></h1>
		
		<div class="entry-meta">
			<?php
			$categories = get_the_term_list($post->ID, 'deals_category', 'Categorie: ', ', ');
			print_r($categories);
			?>
		</div>

	</header>

	<!-- START Single Deal -->
	<div id="deal-details">
		<!-- START Deal Header -->
		<div class="row deal-header">
			<div class="col-md-7 mainimg">
				<?php single_deal_image(); ?>
			</div>
			<div class="col-md-5 buyinfo">
				<div class="row">
					<div class="col-md-12 buydeal">
						<?php if(is_expired(get_field('deal_end_date')) == "no") { ?>
						<a href="<?php echo the_field('affiliate_link'); ?>" title="<?php the_title(); ?>" target="_blank">Visualizza Offerta</a>
						<?php } ?>
					</div>
					<div class="col-md-12 expireinfo">
						<p>Disponibile per: <span id="timer"></span></p>
						<?php countdown(get_field('deal_end_date')); ?>	
					</div>
					<?php if(is_expired(get_field('deal_end_date')) == "no") {
					$catFlag = inCategory();
					if($catFlag == 1) { ?>		
						<div class="col-md-12 priceinfo">
							<?php
							$coupon_discount = get_field('coupon_discount');
							if(empty($coupon_discount)) {
								$orig_price = get_field('original_price');
								$disc_price = get_field('discounted_price');
								if($orig_price != "0" AND $orig_price != "") { ?>
									<div class="row">
										<?php if($disc_price != NULL) { ?>
											<div class="col-sm-12 col-md-4">
												<p>Valore: <span class="orig"><?php echo $orig_price; ?> &euro;</span></p>
											</div>
											<div class="col-sm-12 col-md-4">
												<p>Per te a:
													<span class="disc">
													<?php echo $disc_price . " &euro;"; ?>
													</span>
												</p>
											</div>
											<div class="col-sm-12 col-md-4">
												<p>Risparmi: <span class="save"><?php echo savings($orig_price, $disc_price); ?>%</span></p>
											</div>
										<?php } else { ?>
											<div class="col-sm-12 col-md-4">
												<p>Prezzo:
													<span class="disc">
													<?php echo $orig_price . " &euro;"; ?>
													</span>
												</p>
											</div>
										<?php } ?>
									</div>
								<?php }
							} else { ?>
								<div class="row">
									<div class="col-sm-12 coupon_discount">
										Buono sconto da: <scan class="cdiscount"><?php echo $coupon_discount; ?> &euro;</span>
									</div>
								</div>
							<?php } ?>
						</div>
					<?php }
					} ?>
				</div>
			</div>
		</div> <!-- END Deal Header -->
		<!-- START Deal Content -->
		<div class="deal-content">
			<div class="extrainfo">
				<?php $coupon = getCouponCode();
				if(!empty($coupon)) { ?>
				<p><big>Il codice da utilizzare per aderire all'offerta é: <b><?php echo $coupon; ?></b></big></p>
				<?php } ?>
			</div>
			<?php
			$string = get_field('deal_description');
			echo html_entity_decode($string, ENT_QUOTES, "ISO-8859-8");
			?>
		</div> <!-- END Deal Content -->
	</div> <!-- END Single Deal -->

	<div class="subscribe-newsletter">
		<p>Per non perdere tutte le nostre offerte iscriviti alla nostra newsletter</p>
		<?php subscribe_newsletter_horizontal(); ?>
	</div>

	<footer class="entry-meta">
		<?php echo do_shortcode('[facebookall_comments]'); ?>
		<?php echo do_shortcode('[facebookall_share]'); ?>
		<?php unite_setPostViews(get_the_ID()); ?>
	</footer>
</article>
