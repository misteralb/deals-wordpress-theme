<?php
/*
 Template: 	Content Constructor
 Modified: 	26/01/2015
 Author:	Arber Braja
*/
?>

<?php
$post_type = get_post_type();
if($post_type == 'post') { get_template_part('content', 'posts_main'); }
else if($post_type == 'deals') { get_template_part('content', 'deals_main'); }