<?php
/*
 Template Name:		Contest Page
 Modified:			05/02/2015
 Author:			Arber Braja
*/
?>

<?php get_header(); ?>

	<div id="primary" class="content-area col-sm-12 col-md-12 <?php echo of_get_option('site_layout'); ?>">
		<main id="main" class="site-main" role="main">

			<?php echo do_shortcode('[contest_content_september_2015]'); ?>

		</main>
	</div>

<?php get_footer(); ?>
