<?php
/*
 Template: 	Single Post
 Modified: 	26/01/2015
 Author:	Arber Braja
*/
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header page-header">
		<?php the_post_thumbnail('unite-featured', array('class' => 'thumbnail')); ?>
		<h1 class="entry-title "><?php the_title(); ?></h1>
	</header>

	<div class="entry-content">
		<?php
			the_content();
			wp_link_pages(array('before' => '<div class="page-links">' . __('Pagine:', 'wpdeals'), 'after'  => '</div>'));
		?>
	</div>

	<footer class="entry-meta">
		<?php unite_setPostViews(get_the_ID()); ?>
	</footer>
</article>
